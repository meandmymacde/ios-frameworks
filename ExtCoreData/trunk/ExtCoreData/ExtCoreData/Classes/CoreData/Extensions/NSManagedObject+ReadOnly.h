//
//  NSManagedObject+ReadOnly.h
//  ExtCoreData
//
//  Created by Thomas Bonk on 19.09.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (ReadOnly)

#pragma mark - Read Only Status

- (BOOL)isReadOnly;

@end
