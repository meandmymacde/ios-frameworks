//
//  NSManagedObject+ReadOnly.m
//  ExtCoreData
//
//  Created by Thomas Bonk on 19.09.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import "NSManagedObject+ReadOnly.h"

@implementation NSManagedObject (ReadOnly)

#pragma mark - Read Only Status

- (BOOL)isReadOnly
{
    XEntry();
    
    NSManagedObjectID *objId = self.objectID;
    NSPersistentStore *store = objId.persistentStore;
    BOOL readOnly = store.isReadOnly;
    
    XRet(readOnly);
}

@end
