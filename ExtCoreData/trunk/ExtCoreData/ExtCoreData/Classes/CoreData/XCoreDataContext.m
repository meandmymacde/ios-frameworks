//
//  XCoreDataContext.m
//  ExtApp
//
//  Created by Thomas Bonk on 15.07.11.
//  Copyright 2011 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XCoreDataContext.h"


@implementation XCoreDataContext

#pragma mark - Properties

@synthesize modelURL=__modelUrl;
@synthesize storeURL=__storeUrl;
@synthesize managedObjectContext=__managedObjectContext;
@synthesize managedObjectModel=__managedObjectModel;
@synthesize persistentStoreCoordinator=__persistentStoreCoordinator;


#pragma mark - Static Methods

+ (bool)persistenceStoreAtUrl:(NSURL *)storeUrl needsMigrationForModelAtUrl:(NSURL *)modelURL {
    
    XEntry();
    
    NSError *error = nil;
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                              URL:storeUrl
                                                                                            error:&error];
    XLogErrorObject(error);
    XLogDebug(NSStringFromClass([self class]), @"sourceMetadata: %@", sourceMetadata);
    
    if (!sourceMetadata) {
        
        return NO;
    }
    
    NSManagedObjectModel *managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSSet *versionIdentifiers = [managedObjectModel versionIdentifiers];
    NSSet *sourceVersionIdentifiers = [NSSet setWithArray:[sourceMetadata objectForKey:NSStoreModelVersionIdentifiersKey]];
    
    XLogDebug(NSStringFromClass([self class]), @"versionIdentifiers = %@", sourceMetadata);
    XLogDebug(NSStringFromClass([self class]), @"sourceVersionIdentifiers = %@", sourceVersionIdentifiers);
    
    bool result = ![sourceVersionIdentifiers isEqualToSet:versionIdentifiers];
    
    //XRelease(managedObjectModel);
    
    XRet(result);
}


#pragma mark - Initialization / Deallocation

- (id)initWithModelURL:(NSURL *)modelUrl persistentStoreURL:(NSURL *)storeUrl {
	
	self = [super init];
	
	if (self) {
		
		__modelUrl = modelUrl;
		__storeUrl = storeUrl;
	}
	
	return self;
}

- (id)initWithCoreDataContext:(XCoreDataContext *)coreDataContext {
	
	self = [super init];
	
	if (self) {
	
		__modelUrl = coreDataContext.modelURL;
		__storeUrl = coreDataContext.storeURL;
		__persistentStoreCoordinator = coreDataContext.persistentStoreCoordinator;
		__managedObjectModel = coreDataContext.managedObjectModel;
	}
	
	return self;
}

- (void)mergeChangesOnSave
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(managedObjectContextDidSave:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:nil];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Observer callbacks

- (void)managedObjectContextDidSave:(NSNotification *)notification {
    
	XLogInfo(NSStringFromClass([self class]), @"Managed object context did save, notification");
	
	if ([notification object] != self.managedObjectContext) {
		
		[self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
	}
}


#pragma mark - Transaction handling

- (BOOL)hasChanges {

	BOOL result = NO;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	
	if (managedObjectContext != nil) {
		
		result = [managedObjectContext hasChanges];
	}
	
	return result;
}

- (void)commit {
	
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	
    if (managedObjectContext != nil) {
		
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			
			NSException *exception = [NSException exceptionWithName:[error domain]
															 reason:[error localizedFailureReason] 
														   userInfo:[NSDictionary dictionaryWithObject:error forKey:@"error"]];

            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            @throw exception;
        } 
    }
}

- (void)rollback {
	
	if ([self hasChanges]) {
		
		NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
		
		[managedObjectContext rollback];
	}
}


#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
	
    if (__managedObjectContext != nil) {
		
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
		
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
	
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (__managedObjectModel != nil) {
		
        return __managedObjectModel;
    }
	
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:self.modelURL];    
	
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (__persistentStoreCoordinator != nil) {
		
        return __persistentStoreCoordinator;
    }
    
    
    NSError *error = nil;
    NSDictionary *options = @{
                                NSMigratePersistentStoresAutomaticallyOption : @(YES),
                                NSInferMappingModelAutomaticallyOption : @(YES)
    };
    
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType 
													configuration:nil 
															  URL:self.storeURL 
														  options:options
															error:&error]) {
		
        NSException *exception = [NSException exceptionWithName:[error domain]
														 reason:[error localizedFailureReason] 
													   userInfo:[NSDictionary dictionaryWithObject:error forKey:@"error"]];
		
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		@throw exception;
    }    
    
    return __persistentStoreCoordinator;
}

@end
