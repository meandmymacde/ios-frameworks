//
//  XCoreDataContext.h
//  ExtApp
//
//  Created by Thomas Bonk on 15.07.11.
//  Copyright 2011 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>


/*!
    @class XCoreDataContext
    @abstract This class povides an easy to use encapsulation of the requeired
              CoreData components Persistend Store Coordinator, Managed Object
              Model and Managed Object Context.
    @discussion
        You have to create a new XCoreDataContext for each thread in your
        application. You can initialize the new instance with a master 
        instance in order to refer to the same Persistent Store Coordinator
        and Managed Object Model.
 */
@interface XCoreDataContext : NSObject

#pragma mark - Properties

/*!
    @var modelURL
    @abstract Getter for the URL of the Managed Object Model.
 */
@property (nonatomic, strong, readonly) NSURL *modelURL;
/*!
    @var storeURL
    @abstract Getter for the URL of the Persistent Store.
 */
@property (nonatomic, strong, readonly) NSURL *storeURL;
/*!
    @var managedObjectContext
    @abstract @Getter for the Managed Object Context.
 */
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
/*!
    @var managedObject Model
    @abstract Getter for the Managed Object Model
 */
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
/*!
    @var persistentStoreCoordinator
    @abstract Getter for the Persistent Store Coordinator.
    @throws NSException
 */
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;


#pragma mark - Static Methods

/*!
    This method checks whether the given persistence store needs migration for
    the given model.
    @param storeUrl
        The URL of the persistence store.
    @param modelURL
        The URL of the model.
    @result YES if the persistence store needs migration; otherwise NO.
 */
+ (bool)persistenceStoreAtUrl:(NSURL *)storeUrl needsMigrationForModelAtUrl:(NSURL *)modelURL;


#pragma mark - Initialization / Deallocation

/*!
    This method initializes a CoreData context with a model URL and a persistent
    store URL.
    @param modelURL
        The URL of the Object Model.
    @param storeURL
        The URL of the Persistent Store.
    @result An initialized instance of the CoreData context or nil in case of 
            an error.
 */
- (id)initWithModelURL:(NSURL *)modelUrl persistentStoreURL:(NSURL *)storeURL;

/*!
    This method initializes a CoreData context with another CoreData context.
    @param coreDataContext
        The CoreData context which is the base for the new instance.
    @discussion The newly created CoreData context refers to the same Managed 
             Object Model and the same Persistent Store Coordinator as the
             base CoreData context. Only a new Managed Object Context is
            created.
    @result An initialized instance of the CoreData context or nil in case of 
            an error.
 */
- (id)initWithCoreDataContext:(XCoreDataContext *)coreDataContext;

/*!
    This method registers this context for the 
    NSManagedObjectContextDidSaveNotification in order to merge changes in
    an multi-threaded application.
 */
- (void)mergeChangesOnSave;


#pragma mark - Transaction handling

/*!
    This method checks whether this context has changes.
    @result YES of the CoreData context has changes, NO else.
 */
- (BOOL)hasChanges;

/*!
    This method commits the changes if there are any.
    @throws NSException
 */
- (void)commit;

/*!
    This method rolls back the changes if there are any.
 */
- (void)rollback;

@end
