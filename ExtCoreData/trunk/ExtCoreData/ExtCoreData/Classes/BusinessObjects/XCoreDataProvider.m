//
//  XManagedObjectContext.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XBusinessObjectProviderProtocol.h"
#import "XCoreDataProvider.h"
#import "XCoreDataContext.h"


@interface XCoreDataProvider (private)

- (void)initializeFetchedResultsController;

@end


@implementation XCoreDataProvider

#pragma mark - Properties

@synthesize fetchedResultsControllerClass;
@synthesize servedEntityName = __servedEntityName;
@synthesize filter = __filter;
@synthesize sortDescriptor = __sortDescriptor;
@synthesize sectionNameKeyPath = __sectionNameKeyPath;
@synthesize delegate = __delegate;
@synthesize context = __context;

- (Class)fetchedResultsControllerClass
{
    return [NSFetchedResultsController class];
}

- (void)setFilter:(NSPredicate *)filterPredicate
{
    __filter = filterPredicate;
    [self initializeFetchedResultsController];
}

- (void)setSortDescriptor:(NSSortDescriptor *)aSortDescriptor
{
    __sortDescriptor = aSortDescriptor;
    [self initializeFetchedResultsController];
}

- (void)setSectionNameKeyPath:(NSString *)aSectionNameKeyPath
{
    __sectionNameKeyPath = aSectionNameKeyPath;
    [self initializeFetchedResultsController];
}

- (XMulticastDelegate<XBusinessObjectProviderDelegateProtocol> *)delegate
{
    XEntry();
    
    if (__delegate == nil) {
        
        __delegate = (XMulticastDelegate<XBusinessObjectProviderDelegateProtocol> *)[[XMulticastDelegate alloc] init];
    }
    
    XRet(__delegate);
}


#pragma mark - Initialization

- (void)initializeFetchedResultsController
{
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:self.servedEntityName
											  inManagedObjectContext:self.context.managedObjectContext];
	
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:self.filter];
    [fetchRequest setFetchBatchSize:5];
    
    // Edit the sort key as appropriate.
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:self.sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController.delegate = nil;
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    _fetchedResultsController = [[self.fetchedResultsControllerClass alloc] initWithFetchRequest:fetchRequest
                                                                            managedObjectContext:self.context.managedObjectContext
                                                                              sectionNameKeyPath:self.sectionNameKeyPath
                                                                                       cacheName:nil];
    _fetchedResultsController.delegate = self;
    [_fetchedResultsController performFetch:nil];
}

- (id)initWithCoreDataContext:(XCoreDataContext *)context
             servedEntityName:(NSString *)entityName
                       filter:(NSPredicate *)aFilter
               sortDescriptor:(NSSortDescriptor *)aSortDescriptor
           sectionNameKeyPath:(NSString *)aSectionNameKeyPath;
{
    XEntry();
    
    if ((self = [super init]) != nil) {
        
        __context = context;
        __filter = aFilter;
        __servedEntityName = entityName;
        __sortDescriptor = aSortDescriptor;
        __delegate = nil;
        __sectionNameKeyPath = aSectionNameKeyPath;
        
        [self initializeFetchedResultsController];
    }
    
    XRet(self);
}

- (void)dealloc
{
    XEntry();
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    XLeave();
}


#pragma mark - Creating and deleting objects

- (id)createObjectForName:(NSString *)entityName
{
    NSManagedObject *managedObject = [NSEntityDescription insertNewObjectForEntityForName:entityName 
                                                                   inManagedObjectContext:self.context.managedObjectContext];
    
    return managedObject;
}

- (void)deleteObject:(id)object
{
    [self.context.managedObjectContext deleteObject:object];
}


#pragma mark - Accessing the objects

- (NSInteger)numberOfSections
{
    NSInteger count = [_fetchedResultsController sections].count;
    
    return count;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section 
{
    NSInteger count = 0;
    NSArray *sections = [_fetchedResultsController sections];
    
    if (sections.count > 0) {
        
        id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
        count = [sectionInfo numberOfObjects];
    }
    
    return count;
}

- (NSArray *)sectionIndexTitles
{
    NSArray *titles = [_fetchedResultsController sectionIndexTitles];
		
	return titles;
}

- (NSArray *)allObjects
{
    return _fetchedResultsController.fetchedObjects;
}

- (id)objectAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    return obj;
}

- (NSIndexPath *)indexPathForObject:(id)object
{
    NSIndexPath *result = [_fetchedResultsController indexPathForObject:object];
    
    return result;
}

- (NSString *)modelIndentifierForElementAtIndexPath:(NSIndexPath *)indexPath
{
    XEntry();
    
    NSString *identifier = nil;
    
    if (indexPath != nil) {
    
        NSManagedObject *managedObject = [self objectAtIndexPath:indexPath];
        
        identifier = [[managedObject.objectID URIRepresentation] absoluteString];
    }
    
    XRet(identifier);
}

- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier
{
    XEntry();
    
    NSIndexPath *indexPath = nil;
    
    if (identifier != nil) {
        
        // get the objectID first from PSC
        NSManagedObjectContext *managedObjectContext = self.context.managedObjectContext;
        NSPersistentStoreCoordinator *persistentStoreCoordinator = managedObjectContext.persistentStoreCoordinator;
        NSURL *modelUrl = [NSURL URLWithString:identifier];
        NSManagedObjectID *objectID = [persistentStoreCoordinator managedObjectIDForURIRepresentation:modelUrl];
        
        if (objectID != nil) {
        
            // then fetch the object from MOC
            NSError *error = nil;
            NSManagedObject *managedObject = [managedObjectContext existingObjectWithID:objectID error:&error];
            
            if (error == nil && managedObject != nil) {
                
                indexPath = [self indexPathForObject:managedObject];
            }
            else {
                
                XLogError(NSStringFromClass([self class]), @"ERROR getting Session object from MOC: %@", [error localizedDescription]);
            }
        }
    }
    
    XRet(indexPath);
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    XEntry();
    
    if (controller == _fetchedResultsController) {
        
        if ([self.delegate respondsToSelector:@selector(providerWillChangeContent:)])
        {
            [self.delegate providerWillChangeContent:self];
        }
    }
    
    XLeave();
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    XEntry();
    
    if (controller == _fetchedResultsController) {
    
        if ([self.delegate respondsToSelector:@selector(provider:
                                                        didChangeObject:
                                                        atIndexPath:
                                                        forChangeType:
                                                        newIndexPath:)])
        {
            [self.delegate provider:self
                    didChangeObject:anObject
                        atIndexPath:indexPath
                      forChangeType:(XBusinessObjectChangeType)type
                       newIndexPath:newIndexPath];
        }
    }
    
    XLeave();
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    XEntry();
    
    if (controller == _fetchedResultsController) {
    
        if ([self.delegate respondsToSelector:@selector(provider:
                                                        didChangeSectionAtIndex:
                                                        forChangeType:)])
        {
            [self.delegate provider:self
            didChangeSectionAtIndex:sectionIndex
                      forChangeType:(XBusinessObjectChangeType)type];
        }
    }
    
    XLeave();
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    XEntry();
    
    if (controller == _fetchedResultsController) {
        
        if ([self.delegate respondsToSelector:@selector(providerWillChangeContent:)])
        {
            [self.delegate providerDidChangeContent:self];
        }
    }
    
    XLeave();
}


#pragma mark - Data change related methods

- (BOOL)isReadOnly
{
    BOOL result = NO;
    
    return result;
}

- (BOOL)hasChanges
{
    BOOL result = self.context.managedObjectContext.hasChanges;
    
    return result;
}

- (BOOL)commit:(NSError **)error
{
    BOOL result = [self.context.managedObjectContext save:error];
    
    return result;
}

- (void)rollback 
{
    [self.context.managedObjectContext rollback];
}

@end
