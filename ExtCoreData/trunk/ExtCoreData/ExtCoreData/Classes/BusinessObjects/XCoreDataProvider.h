//
//  XCoreDataProvider.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <CoreData/CoreData.h>

@protocol XBusinessObjectProviderProtocol;
@class XCoreDataContext;


@interface XCoreDataProvider : NSObject<XBusinessObjectProviderProtocol,
                                        NSFetchedResultsControllerDelegate>
{
    @protected
    NSFetchedResultsController *_fetchedResultsController;
}

#pragma mark - Properties

/*!
 @var fetchedResultsControllerClass
 @abstract This property returns the default class of which fetched results
           controller instances are created. This class must eb a subclass of
           NSFetchedResultsController.
 */
@property (nonatomic, readonly) Class fetchedResultsControllerClass;

/*!
 @var description
 @abstract  A short description of the data which provdied by this provider.
 */
@property (nonatomic, strong, readwrite) NSString *description;

/*!
 @var servedEntityName
 @abstract The name of the entity which shall be served by this provider.
 */
@property (nonatomic, strong, readonly) NSString *servedEntityName;

/*!
 @var filter
 @abstract This is the filter predicate. If filter is nil, all objects are
 returned. Can be nil.
 */
@property (nonatomic, strong) NSPredicate *filter;

/*!
 @var sortDescriptor
 @abstract The sort descriptor.
 */
@property (nonatomic, strong) NSSortDescriptor *sortDescriptor;

/*!
 @var sectionNameKeyPath
 @abstract The key path for the property which provides the key path. Can be 
 nil.
 */
@property (nonatomic, strong, readonly) NSString *sectionNameKeyPath;

/*!
 @var delegate
 @abstract The multicast delegate registrator. Add your delegate here in order
 to get notified. The delegates must implement the
 XBusinessObjectProviderDelegateProtocol.
 */
@property (nonatomic, strong, readonly) XMulticastDelegate<XBusinessObjectProviderDelegateProtocol> *delegate;

/*!
 @var context
 @abstract The CoreData context.
 */
@property (nonatomic, strong, readonly) XCoreDataContext *context;


#pragma mark - Initialization

- (id)initWithCoreDataContext:(XCoreDataContext *)context
             servedEntityName:(NSString *)entityName
                       filter:(NSPredicate *)aFilter
               sortDescriptor:(NSSortDescriptor *)aSortDescriptor
           sectionNameKeyPath:(NSString *)aSectionNameKeyPath;

@end
