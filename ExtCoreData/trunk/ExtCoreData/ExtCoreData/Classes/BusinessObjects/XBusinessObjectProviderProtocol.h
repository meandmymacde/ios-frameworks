//
//  XBusinessObjectProviderProtocol.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XBusinessObjectProviderProtocol;

typedef NS_ENUM(NSUInteger, XBusinessObjectChangeType)
{
    XBusinessObjectChangeInsert = 1,
	XBusinessObjectChangeDelete = 2,
	XBusinessObjectChangeMove   = 3,
	XBusinessObjectChangeUpdate = 4
};

@protocol XBusinessObjectProviderDelegateProtocol <NSObject>

@optional
- (void)providerWillChangeContent:(id<XBusinessObjectProviderProtocol>)provider;

@optional
- (void)providerDidChangeContent:(id<XBusinessObjectProviderProtocol>)provider;

@optional
- (void)provider:(id<XBusinessObjectProviderProtocol>)provider
 didChangeObject:(id)anObject
     atIndexPath:(NSIndexPath *)indexPath
   forChangeType:(XBusinessObjectChangeType)type
    newIndexPath:(NSIndexPath *)newIndexPath;

@optional
-  (void)provider:(id<XBusinessObjectProviderProtocol>)provider
 didChangeSectionAtIndex:(NSUInteger)sectionIndex
    forChangeType:(XBusinessObjectChangeType)type;

@end


@protocol XBusinessObjectProviderProtocol<NSObject>

@required

#pragma mark - Properties

/*!
 @var description
 @abstract  A short description of the data which provdied by this provider.
 */
@property (nonatomic, strong, readwrite) NSString *description;

/*!
 @var servedEntityName
 @abstract The name of the entity which shall be served by this provider.
 */
@property (nonatomic, strong, readonly) NSString *servedEntityName;

/*!
 @var filter
 @abstract This is the filter predicate. If filter is nil, all objects are
 returned. Can be nil.
 */
@property (nonatomic, strong) NSPredicate *filter;

/*!
 @var sortDescriptor
 @abstract The sort descriptor.
 */
@property (nonatomic, strong) NSSortDescriptor *sortDescriptor;

/*!
 @var sectionNameKeyPath
 @abstract The key path for the property which provides the key path. Can be
 nil.
 */
@property (nonatomic, strong, readonly) NSString *sectionNameKeyPath;

/*!
 @var delegate
 @abstract The multicast delegate registrator. Add your delegate here in order
        to get notified. The delegates must implement the 
        XBusinessObjectProviderDelegateProtocol.
 */
@property (nonatomic, strong, readonly) XMulticastDelegate<XBusinessObjectProviderDelegateProtocol> *delegate;


#pragma mark - Creating and deleting objects

/*!
 Creates an object for the given entity name.
 */
- (id)createObjectForName:(NSString *)entityName;

/*!
 Deletes the given object.
 */
- (void)deleteObject:(id)object;


#pragma mark - Accessing the objects

/*!
 Returns the number of sections. 
 @result The number of sections or 1 if there are no sections.
 */
- (NSInteger)numberOfSections;

/*!
 Returns the number of rows in a given section.
 @param section
    An index number identifying a section.
 @result The number of rows in section.
 */
- (NSInteger)numberOfRowsInSection:(NSInteger)section;

/*!
 Asks the provider to return the titles for the sections.
 @result An array of strings that serve as the title of sections. For example, 
 for an alphabetized list, you could return an array containing strings “A” 
 through “Z”.
 */
- (NSArray *)sectionIndexTitles;

/*!
 All objects.
 @result An array with all objects.
 */
- (NSArray *)allObjects;

/*!
 Returns the object at the given index path.
 @param indexPath
    An index path in the fetch results. If indexPath does not describe a valid 
    index path in the fetch results, an exception is raised.
 @result The object at a given index path in the fetch results.
 */
- (id)objectAtIndexPath:(NSIndexPath *)indexPath;

/*!
 Returns the index path of a given object.
 @param object
    An object in the receiver’s fetch results.
 @result The index path of object in the receiver’s fetch results, or nil if 
 object could not be found.
 */
- (NSIndexPath *)indexPathForObject:(id)object;

/*!
 Returns the string that uniquely identifies the data at the specified location.
 @param indexPath
    The index path to the requested data object.
 @result A string that uniquely identifies the data object.
 */
- (NSString *)modelIndentifierForElementAtIndexPath:(NSIndexPath *)indexPath;

/*!
 Returns the current index of the data object with the specified identifier.
 @param identifier
    The identifier for the requested data object. Use this identifier to locate 
    the matching object in your data source object. This is the same string that 
    your app’s modelIdentifierForElementAtIndexPath:inView: method returned when 
    encoding the data originally.
 @result The current index of the object whose data matches the value in 
    identifier, or nil if the object was not found.
 */
- (NSIndexPath *)indexPathForElementWithModelIdentifier:(NSString *)identifier;


#pragma mark - Data change related methods

/*!
 This method checks whether the Business Object provider is Read-Only or not.
 @result YES if the Business Object provider is read-Only, NO otherwise.
 */
- (BOOL)isReadOnly;

/*!
 This method returns a flag which determines whether this provider has changes
 which haven't been committed, yet.
 @result YES if there are changes, NO otherwise.
 */
- (BOOL)hasChanges;

/*!
 Attempts to commit unsaved changes to objects.
 @param error
    A pointer to an NSError object. You can pass nil.
 @result YES if the save succeeds, otherwise NO.
 */
- (BOOL)commit:(NSError **)error;

/*!
 Removes everything from the undo stack, discards all insertions and deletions, 
 and restores updated objects to their last committed values.
 */
- (void)rollback;

@end
