//
//  ExtCoreData.h
//  ExtCoreData
//
//  Created by Thomas Bonk on 10.06.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <ExtCoreData/XBusinessObjectProviderProtocol.h>
#import <ExtCoreData/XCoreDataProvider.h>
#import <ExtCoreData/XCoreDataContext.h>
#import <ExtCoreData/NSManagedObject+ReadOnly.h>

