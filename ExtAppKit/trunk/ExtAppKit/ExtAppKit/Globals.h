//
//  Globals.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 31.05.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#ifndef ExtAppKit_Globals_h
#define ExtAppKit_Globals_h

#define XRelease(obj) { [obj release]; obj= nil; }

#define I18N(str) NSLocalizedString(str, nil)
#define I18NT(str,tab) NSLocalizedStringFromTable(str, tab, nil)

#define XSuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

#endif
