//
//  XUserSettings.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 17.01.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XUserSettings.h"

// The suffix for the original values
static NSString * const OrigSuffix = @"_____orig_____";


@implementation XUserSettings

#pragma mark - Class variables

static BOOL trackingChangedUserDefaults = NO;
static NSMutableDictionary *changeListeners = nil;


#pragma mark - Initialization

+ (void)initialize
{
    changeListeners = [NSMutableDictionary dictionary];
}

+ (void)registerDefaults:(NSDictionary *)defaults
{
    XEntry();
    
	NSUserDefaults      *userDefaults      = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaultsWithOrigs = [NSMutableDictionary dictionaryWithDictionary:defaults];
    
    // add entries for original/old values in order to
    // find out which values have changed
    for (NSString *key in defaults.allKeys) {
        
        NSString *oldKey = [key stringByAppendingString:OrigSuffix];
        id       value   = [defaults objectForKey:key];
        
        [defaultsWithOrigs setObject:value forKey:oldKey];
    }
    
    XLogDebug(@"XUserSettings", @"User defaults: %@", defaultsWithOrigs);

	[userDefaults registerDefaults:defaultsWithOrigs];
    
    XLeave();
}


#pragma mark - Handle Change of User Defaults

+ (NSArray *)changedPreferences:(NSUserDefaults *)defaults
{
    // Get the user defaults
    NSDictionary *preferences = [defaults dictionaryRepresentation];
    NSMutableArray *changedKeys = [NSMutableArray array];
    
    for (NSString *key in preferences.allKeys) {
        
        @autoreleasepool {
            
            if (![key hasSuffix:OrigSuffix]) {
                
                id value     = [defaults objectForKey:key];
                NSString *origKey = [key stringByAppendingString:OrigSuffix];
                id origValue = [defaults objectForKey:origKey];
                
                XLogDebug(@"XUserSettings", @"Checking: %@ = %@ EQUALS %@ = %@?", key, value, origKey, origValue);
                
                if (![value isEqual:origValue]) {
                    
                    XLogDebug(@"XUserSettings", @"User default >%@< changed", key);
                    
                    // value has changed; save for notification
                    [changedKeys addObject:key];
                    [defaults setObject:value forKey:origKey];
                }
            }
        }
    }
    
    [defaults synchronize];
    
    return changedKeys;
}

+ (void)trackChangedUserDefaults
{
    XEntry();
    
    if (!trackingChangedUserDefaults) {
        
        trackingChangedUserDefaults = YES;
        
        NSArray *changedKeys = [self changedPreferences:[NSUserDefaults standardUserDefaults]];
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        
        for (NSString *key in changedKeys) {
            
            @synchronized(changeListeners) {
                
                NSArray *operations = [[changeListeners objectForKey:key] allObjects];
                
                if (operations != nil) {
                    
                    XLogDebug(@"XUserSettings", @"Enqueueing operations: %@", operations);
                    
                    [mainQueue addOperations:operations waitUntilFinished:NO];
                }
            }
        }
        
        trackingChangedUserDefaults = YES;
    }
    
    XLeave();
}

+ (void)addObserver:(id)observer
           selector:(SEL)notificationSelector
     preferenceName:(NSString *)preferenceName
{
    @synchronized(changeListeners) {
        
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:observer
                                                                                selector:notificationSelector
                                                                                  object:nil];
        NSMutableSet *operations = [changeListeners objectForKey:preferenceName];
        
        if (operations == nil) {
            
            operations = [NSMutableSet set];
            [changeListeners setObject:operations forKey:preferenceName];
        }
        
        [operations addObject:operation];
    }
}

+ (void)removeObserver:(id)observer
              selector:(SEL)notificationSelector
        preferenceName:(NSString *)preferenceName
{
    @synchronized(changeListeners) {
        
        NSMutableSet *operations = [changeListeners objectForKey:preferenceName];
        
        if (operations != nil) {
            
            NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:observer
                                                                                    selector:notificationSelector
                                                                                      object:nil];
            
            [operations removeObject:operation];
        }
    }
}

@end
