//
//  XUserSettings.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 17.01.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XUserSettings : NSObject

#pragma mark - Initialization

+ (void)registerDefaults:(NSDictionary *)defaults;


#pragma mark - Tracking Changes of User Defaults

+ (void)trackChangedUserDefaults;
+ (void)addObserver:(id)observer
           selector:(SEL)notificationSelector
     preferenceName:(NSString *)preferenceName;
+ (void)removeObserver:(id)observer
              selector:(SEL)notificationSelector
        preferenceName:(NSString *)preferenceName;

@end
