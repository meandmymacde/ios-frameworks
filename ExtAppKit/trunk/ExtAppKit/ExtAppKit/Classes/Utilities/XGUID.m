//
//  XGUID.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XGUID.h"

@implementation XGUID

+ (NSString *)guid
{
    XEntry();
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    
    CFRelease(theUUID);
    XRet((NSString *)CFBridgingRelease(string));
}

@end
