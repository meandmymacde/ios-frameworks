//
//  XMulticastDelegate.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 11.03.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @class XMulticastDelegate
 @abstract This class handles messages sent to delegates, multicasting these 
           messages to multiple observers.
           Taken from this blog article:
           http://www.scottlogic.co.uk/blog/colin/2012/11/a-multicast-delegate-pattern-for-ios-controls/
 */
@interface XMulticastDelegate : NSObject

/*!
 This method adds the given delegate implementation to the list of observers.
 @param delegate
    The delegate to be added.
 */
- (void)addDelegate:(id)delegate;

/*!
 This method removes the given delegate implementation to the list of observers.
 @param delegate
    The delegate to be removed.
 */
- (void)removeDelegate:(id)delegate;

@end
