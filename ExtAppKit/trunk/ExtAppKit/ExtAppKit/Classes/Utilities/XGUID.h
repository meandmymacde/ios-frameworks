//
//  XGUID.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGUID : NSObject

/*!
 This method return a GUID string.
 @result A string with a GUID.
 */
+ (NSString *)guid;

@end
