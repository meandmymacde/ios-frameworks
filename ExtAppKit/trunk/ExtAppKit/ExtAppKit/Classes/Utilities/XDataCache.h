//
//  XDataCache.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XDataCache;

@protocol XDataCacheCallbackProtocol

@required

- (id)dataCache:(XDataCache *)dataCache
dataForCategory:(NSString *)category
            key:(id<NSObject>)key
       dataSize:(NSUInteger *)dataSize;

@end

@interface XDataCache : NSObject

#pragma mark -- Singleton

+ (XDataCache *)sharedCache;


#pragma -- Properties

@property (nonatomic, assign) NSUInteger totalCostLimit;


#pragma mark -- Manage cached data

- (id)cachedDataForCategory:(NSString *)category
                        key:(id<NSObject>)key
                   callback:(id<XDataCacheCallbackProtocol>)callback;

- (void)removeDataForCategory:(NSString *)category
                          key:(id<NSObject>)key;

- (void)removeDataForCategory:(NSString *)category;


#pragma mark -- Cache management

- (void)removeAllObjects;

@end
