//
//  XDevice.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 11.05.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XDevice : NSObject

#pragma mark - Device Identification

/*!
    This method return a unique device identifier.
    @discuss This unique device identifier can be used safely since it doesn't
             use deprecated methods.
 */
+ (NSString *)deviceId;

@end
