//
//  XMulticastDelegate.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 11.03.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XMulticastDelegate.h"

@implementation XMulticastDelegate
{
    // the array of observing delegates
    NSMutableArray* _delegates;
}


#pragma mark - Initialization

- (id)init
{
    XEntry();
    
    if (self = [super init])
    {
        _delegates = [NSMutableArray array];
    }
    
    XRet(self);
}


#pragma mark - Add and remove delegates

- (void)addDelegate:(id)delegate
{
    XEntry();
    
    [self willChangeValueForKey:@"delegates"];
    [_delegates addObject:delegate];
    [self didChangeValueForKey:@"delegates"];
    
    XLeave();
}

- (void)removeDelegate:(id)delegate
{
    XEntry();
    
    [self willChangeValueForKey:@"delegates"];
    [_delegates removeObject:delegate];
    [self didChangeValueForKey:@"delegates"];
    
    XLeave();
}


#pragma mark - Method forwarding

- (BOOL)respondsToSelector:(SEL)aSelector
{
    XEntry();
    
    if ([super respondsToSelector:aSelector]) {
        
        XRet(YES);
    }
    
    // if any of the delegates respond to this selector, return YES
    for (id delegate in _delegates)
    {
        if ([delegate respondsToSelector:aSelector])
        {
            XRet(YES);
        }
    }
    
    XRet(NO);
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    XEntry();
    
    // can this class create the signature?
    NSMethodSignature* signature = [super methodSignatureForSelector:aSelector];
    
    // if not, try our delegates
    if (signature == nil)
    {
        for (id delegate in _delegates)
        {
            if ([delegate respondsToSelector:aSelector])
            {
                XRet([delegate methodSignatureForSelector:aSelector]);
            }
        }
    }
    
    XRet(signature);
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    XEntry();
    
    // forward the invocation to every delegate
    for (id delegate in _delegates)
    {
        if ([delegate respondsToSelector:[anInvocation selector]])
        {
            [anInvocation invokeWithTarget:delegate];
        }
    }
    
    XLeave();
}

@end
