//
//  XDevice.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 11.05.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XDevice.h"
#import "NSString+MD5.h"


@implementation XDevice

#pragma mark - Device Identification

+ (NSString *)deviceId 
{
    NSUUID *uuid = [[UIDevice currentDevice] identifierForVendor];
    NSString *devId = [uuid UUIDString];
    
    return devId;
}

@end
