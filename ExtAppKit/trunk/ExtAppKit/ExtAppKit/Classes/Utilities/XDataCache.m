//
//  XDataCache.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XDataCache.h"

// Maximum number fo bytes in the cache
#define MAX_BYTES (1024 * 1024 * 16) // 4 MB


@implementation XDataCache
{
    NSMutableDictionary *_cacheDictionary;
}


#pragma mark -- Singleton Implementation

static XDataCache *sharedInstance = nil;

+ (XDataCache *)sharedCache
{
    XEntry();
    
    @synchronized(self) {
        
        if (sharedInstance == nil) {
            
            sharedInstance = [[XDataCache alloc] init];
        }
    }
    
    XRet(sharedInstance);
}


+ (id)allocWithZone:(NSZone *)zone
{
    XEntry();
    
    @synchronized(self) {
        
        if (sharedInstance == nil) {
            
            sharedInstance = [super allocWithZone:zone];
            
            return sharedInstance;  // assignment and return on first allocation
        }
    }
    
    XRet(nil); // on subsequent allocation attempts return nil
}


- (id)copyWithZone:(NSZone *)zone
{
    XEntry();
    
    XRet(self);
}

- (id)init
{
    XEntry();
    
    if ((self = [super init]) != nil) {
        
        _cacheDictionary = [NSMutableDictionary new];
        self.totalCostLimit = MAX_BYTES;
    }
    
    XRet(self);
}


#pragma -- Properties

@synthesize totalCostLimit = _totalCostLimit;

- (void)setTotalCostLimit:(NSUInteger)value
{
    XEntry();
    
    _totalCostLimit = value;
    [self adjustTotalCostLimit];
    
    XLeave();
}


#pragma mark -- Get cached data

- (void)adjustTotalCostLimit
{
    XEntry();
    
    if (_cacheDictionary.count > 0) {
        
        NSUInteger limitPerCache = self.totalCostLimit / _cacheDictionary.count;
        
        [_cacheDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
           
            NSCache *cache = obj;
            
            cache.totalCostLimit = limitPerCache;
        }];
    }
    
    XLeave();
}

- (NSCache *)cacheForCategory:(NSString *)category
{
    XEntry();
    
    NSCache *cache = _cacheDictionary[category];
    
    if (cache == nil) {
        
        cache = [[NSCache alloc] init];
        _cacheDictionary[category] = cache;
        [self adjustTotalCostLimit];
    }
    
    XRet(cache);
}

- (id)cachedDataForCategory:(NSString *)category
                        key:(id<NSObject>)key
                   callback:(id<XDataCacheCallbackProtocol>)callback
{
    XEntry();
    
    NSCache *categoryCache = [self cacheForCategory:category];
    id data = [categoryCache objectForKey:key];
    NSUInteger size = 0;
    
    if (data == nil) {
        
        data = [callback dataCache:self dataForCategory:category key:key dataSize:&size];
        
        if (data != nil && size > 0) {
            
            [categoryCache setObject:data forKey:key cost:size];
        }
    }
    
    XRet(data);
}

- (void)removeDataForCategory:(NSString *)category
                          key:(id<NSObject>)key
{
    XEntry();
    
    NSCache *categoryCache = [self cacheForCategory:category];
    
    [categoryCache removeObjectForKey:key];
    
    XLeave();
}

- (void)removeDataForCategory:(NSString *)category
{
    XEntry();
    
    [_cacheDictionary removeObjectForKey:category];
    
    XLeave();
}


#pragma mark -- Cache management

- (void)removeAllObjects
{
    XEntry();
    
    [_cacheDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        NSCache *cache = (NSCache *)obj;
        
        [cache removeAllObjects];
    }];
    
    [_cacheDictionary removeAllObjects];
    
    XLeave();
}

@end
