//
//  XSelectorMap.m
//  ExtAppKit
//
//  Created by Bonk, Thomas on 03.01.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XSelectorMap.h"

@implementation XSelectorMap
{
    NSMutableDictionary *__map;
}


#pragma mark - Initialization

- (id)init
{
    XEntry();
    
    self = [super init];
    if (self != nil) {
        
        __map = [NSMutableDictionary new];
    }
    
    XRet(self);
}

- (void)dealloc
{
    XEntry();
    
    [__map removeAllObjects];
    __map = nil;
    
    XLeave();
}


#pragma mark - Add/replace, remove selectors

- (void)addSelector:(SEL)sel forKey:(id<NSCopying>)key
{
    XEntry();
    
    NSString *selectorString = NSStringFromSelector(sel);
    
    [__map setObject:selectorString forKey:key];
    
    XLeave();
}

- (void)removeSelectorForKey:(id)key
{
    XEntry();
    
    [__map removeObjectForKey:key];
    
    XLeave();
}

- (SEL)selectorForKey:(id)key
{
    XEntry();
    
    SEL sel = nil;
    NSString *selectorString = (NSString *)[__map objectForKey:key];
    
    if (selectorString != nil) {
        
        sel = NSSelectorFromString(selectorString);
    }
    
    XRet(sel);
}


#pragma mark - Invoke selector for a key

- (BOOL)checkForReturnValueOfTarget:(id)target andSelector:(SEL)selector
{
    XEntry();
    
    BOOL rc = NO;
    Class cls = [target class];
    Method method = class_getInstanceMethod(cls, selector);
    char returnType[128];
    
    method_getReturnType(method, returnType, sizeof(returnType));
    
    rc = (strcmp("v", returnType) != 0);
    
    XRet(rc);
}

- (id)invokeSelectorForKey:(id)key onTarget:(id)target
{

    XEntry();
    
    id result = nil;
    SEL selector = [self selectorForKey:key];
    
    if (selector == nil) {
        
        XLogWarn(@"XSelectorMap", @"No selector for key >%@< found.", key);
    }
    else {
        
        if (![target respondsToSelector:selector]) {
            
            XLogWarn(@"XSelectorMap", @"Target >%@< doesn't respond to selector >%@<.", target, NSStringFromSelector(selector));
        }
        else {

            XLogDebug(@"XSelectorMap", @"Calling selector >%@< for target >%@<.", NSStringFromSelector(selector), target);
            
            if ([self checkForReturnValueOfTarget:target andSelector:selector]) {
            
                XSuppressPerformSelectorLeakWarning(result = [target performSelector:selector];);
            }
            else {
                
                XSuppressPerformSelectorLeakWarning([target performSelector:selector];);
            }
        }
    }
    
    XRet(result);
}

- (id)invokeSelectorForKey:(id)key onTarget:(id)target withObject:(id)object
{
    
    XEntry();
    
    id result = nil;
    SEL selector = [self selectorForKey:key];
    
    if (selector == nil) {
        
        XLogWarn(@"XSelectorMap", @"No selector for key >%@< found.", key);
    }
    else {
        
        if (![target respondsToSelector:selector]) {
            
            XLogWarn(@"XSelectorMap", @"Target >%@< doesn't respond to selector >%@<.", target, NSStringFromSelector(selector));
        }
        else {
            
            XLogDebug(@"XSelectorMap", @"Calling selector >%@< for target >%@<.", NSStringFromSelector(selector), target);
            if ([self checkForReturnValueOfTarget:target andSelector:selector]) {
             
                XSuppressPerformSelectorLeakWarning(result = [target performSelector:selector
                                                                          withObject:object];);
            }
            else {

                XSuppressPerformSelectorLeakWarning([target performSelector:selector
                                                                 withObject:object];);
            }
        }
    }
    
    XRet(result);
}

- (id)invokeSelectorForKey:(id)key
                    onTarget:(id)target
                  withObject:(id)object1
                  withObject:(id)object2
{
    
    XEntry();
    
    id result = nil;
    SEL selector = [self selectorForKey:key];
    
    if (selector == nil) {
        
        XLogWarn(@"XSelectorMap", @"No selector for key >%@< found.", key);
    }
    else {
        
        if (![target respondsToSelector:selector]) {
            
            XLogWarn(@"XSelectorMap", @"Target >%@< doesn't respond to selector >%@<.", target, NSStringFromSelector(selector));
        }
        else {
            
            XLogDebug(@"XSelectorMap", @"Calling selector >%@< for target >%@<.", NSStringFromSelector(selector), target);
            
            if ([self checkForReturnValueOfTarget:target andSelector:selector]) {
                
                XSuppressPerformSelectorLeakWarning(result = [target performSelector:selector
                                                                          withObject:object1
                                                                          withObject:object2];);
            }
            else {
                
                XSuppressPerformSelectorLeakWarning([target performSelector:selector
                                                                 withObject:object1
                                                                 withObject:object2];);
            }
            
        }
    }
    
    XRet(result);
}

- (id)invokeSelectorForKey:(id)key
                    onTarget:(id)target
               withArguments:(id)arg1, ...
{
    XEntry();
    
    id result = nil;
    SEL selector = [self selectorForKey:key];
    va_list args;
    
    va_start(args, arg1);
    
    if (selector == nil) {
        
        XLogWarn(@"XSelectorMap", @"No selector for key >%@< found.", key);
    }
    else {
        
        if (![target respondsToSelector:selector]) {
            
            XLogWarn(@"XSelectorMap", @"Target >%@< doesn't respond to selector >%@<.", target, NSStringFromSelector(selector));
        }
        else {
            
            NSMethodSignature *sig = [target methodSignatureForSelector:selector];
            NSInvocation *inv = [NSInvocation invocationWithMethodSignature:sig];
            
            [inv setSelector:selector];
            [inv setTarget:target];
            
            int i = 2;
            for (id arg = arg1; arg != nil; arg = va_arg(args, id))
            {
                [inv setArgument:&(arg) atIndex:i]; //arguments 0 and 1 are self and _cmd respectively, automatically set by NSInvocation
                i++;
            }
            
            va_end(args);
            
            XLogDebug(@"XSelectorMap", @"Calling selector >%@< for target >%@<.", NSStringFromSelector(selector), target);
            [inv invoke];
            
            if ([self checkForReturnValueOfTarget:target andSelector:selector]) {
                
                [inv getReturnValue:&result];
            }
        }
    }
    
    XRet(result);
}

- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                         waitUntilDone:(BOOL)waitUntilDone
{
    
    XEntry();
    
    id __block result = nil;
    
    if (waitUntilDone && [[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        
        result = [self invokeSelectorForKey:key onTarget:target];
    }
    else {
        
        NSOperationQueue *mainQ = [NSOperationQueue mainQueue];
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
            
            result = [self invokeSelectorForKey:key onTarget:target];
        }];
        
        [mainQ addOperations:@[op] waitUntilFinished:waitUntilDone];
    }
    
    XRet(result);
}

- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                            withObject:(id)object
                         waitUntilDone:(BOOL)waitUntilDone
{
    XEntry();
    
    id __block result = nil;
    
    if (waitUntilDone && [[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        
        result = [self invokeSelectorForKey:key
                                   onTarget:target
                                 withObject:object];
    }
    else {
        
        NSOperationQueue *mainQ = [NSOperationQueue mainQueue];
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
            
            result = [self invokeSelectorForKey:key
                                       onTarget:target
                                     withObject:object];
        }];
        
        [mainQ addOperations:@[op] waitUntilFinished:waitUntilDone];
    }
    
    XRet(result);
}

- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                            withObject:(id)object1
                            withObject:(id)object2
                         waitUntilDone:(BOOL)waitUntilDone
{
    
    XEntry();
    
    id __block result = nil;
    
    if (waitUntilDone && [[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        
        result = [self invokeSelectorForKey:key
                                   onTarget:target
                                 withObject:object1
                                 withObject:object2];
    }
    else {
        
        NSOperationQueue *mainQ = [NSOperationQueue mainQueue];
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
            
            result = [self invokeSelectorForKey:key
                                       onTarget:target
                                     withObject:object1
                                     withObject:object2];
        }];
        
        [mainQ addOperations:@[op] waitUntilFinished:waitUntilDone];
    }
    
    XRet(result);
}

@end
