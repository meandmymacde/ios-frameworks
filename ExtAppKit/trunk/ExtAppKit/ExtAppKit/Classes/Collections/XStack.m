//
//  XStack.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 02.12.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XStack.h"

@implementation XStack
{
    NSMutableArray *__stack;
}

#pragma mark - Initialization

- (instancetype)init
{
    XEntry();
    
    self = [super init];
    if (self) {
        
        __stack = [NSMutableArray new];
    }
    
    XRet(self);
}


#pragma arguments - Stack Operations

- (NSUInteger)stackSize
{
    XEntry();
    
    NSUInteger size = [__stack count];
    
    XRet(size);
}

- (void)push:(id)object
{
    XEntry();
    
    [__stack addObject:object];
    
    XLeave();
}

- (instancetype)pop
{
    XEntry();
    
    id object = nil;
    
    if ([self stackSize] > 0) {

        object = [self topOfStack];
        [__stack removeLastObject];
    }
    
    XRet(object);
}

- (instancetype)topOfStack
{
    XEntry();
    
    id object = nil;
    
    if ([self stackSize] > 0) {
        
        object = [__stack lastObject];
    }
    
    XRet(object);
}

- (instancetype)secondOfStack
{
    XEntry();
    
    id object = nil;
    
    if ([self stackSize] > 1) {
        
        object = [__stack objectAtIndex:(__stack.count - 2)];
    }
    
    XRet(object);
}

- (void)clear
{
    XEntry();
    
    [__stack removeAllObjects];
    
    XLeave();
}


@end
