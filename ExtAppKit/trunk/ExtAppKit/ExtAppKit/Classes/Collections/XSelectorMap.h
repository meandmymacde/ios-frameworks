//
//  XSelectorMap.h
//  ExtAppKit
//
//  Created by Bonk, Thomas on 03.01.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSelectorMap : NSObject

#pragma mark - Add, replace, remove selectors

- (void)addSelector:(SEL)sel forKey:(id<NSCopying>)key;
- (void)removeSelectorForKey:(id)key;
- (SEL)selectorForKey:(id)key;


#pragma mark - Invoke selector for a key

- (id)invokeSelectorForKey:(id)key onTarget:(id)target;
- (id)invokeSelectorForKey:(id)key onTarget:(id)target withObject:(id)object;
- (id)invokeSelectorForKey:(id)key
                  onTarget:(id)target
                withObject:(id)object1
                withObject:(id)object2;
- (id)invokeSelectorForKey:(id)key
                  onTarget:(id)target
             withArguments:(id)arg1, ... NS_REQUIRES_NIL_TERMINATION;

- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                         waitUntilDone:(BOOL)waitUntilDone;
- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                            withObject:(id)object
                         waitUntilDone:(BOOL)waitUntilDone;
- (id)invokeSelectorOnMainThreadForKey:(id)key
                              onTarget:(id)target
                            withObject:(id)object1
                            withObject:(id)object2
                         waitUntilDone:(BOOL)waitUntilDone;

@end
