//
//  XStack.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 02.12.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XStack : NSObject

#pragma arguments - Stack Operations

- (NSUInteger)stackSize;
- (void)push:(id)object;
- (instancetype)pop;
- (instancetype)topOfStack;
- (instancetype)secondOfStack;
- (void)clear;

@end
