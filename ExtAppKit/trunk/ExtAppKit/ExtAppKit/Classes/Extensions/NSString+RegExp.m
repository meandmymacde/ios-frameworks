//
//  NSString+RegExp.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSString+RegExp.h"

@implementation NSString (RegExp)

- (BOOL)contains:(NSRegularExpression *)regexpr {
 
    XEntry();
    
	NSInteger count = [regexpr numberOfMatchesInString:self
											   options:NSMatchingReportCompletion
												 range:NSMakeRange(0, [self length])];
	
	XRet(count > 0);
}

- (NSString *)stringByReplacing:(NSRegularExpression *)regexpr with:(NSString *)pattern {
    
    XEntry();
    
	NSString *str = [regexpr stringByReplacingMatchesInString:self
													  options:NSMatchingReportCompletion
														range:NSMakeRange(0, [self length])
												 withTemplate:pattern];
	
	XRet(str);
}

@end
