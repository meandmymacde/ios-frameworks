//
//  NSString+Whitespaces.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSString+Whitespaces.h"

@implementation NSString (Whitespaces)

- (NSInteger)trimmedLength
{
    XEntry();
    
    NSString *trimmed = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSInteger length = trimmed.length;
    
    XRet(length);
}

@end
