//
//  NSObject+ImplementsProtocol.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 20.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ImplementsProtocol)

- (BOOL)implementsProtocol:(Protocol *)aProtocol;

@end
