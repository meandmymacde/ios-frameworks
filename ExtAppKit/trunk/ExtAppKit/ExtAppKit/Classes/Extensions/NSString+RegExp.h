//
//  NSString+RegExp.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RegExp)

- (BOOL)contains:(NSRegularExpression *)regexpr;
- (NSString *)stringByReplacing:(NSRegularExpression *)regexpr with:(NSString *)pattern;

@end
