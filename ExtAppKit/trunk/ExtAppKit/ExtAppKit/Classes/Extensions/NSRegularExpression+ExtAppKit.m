//
//  NSRegularExpression+ExtAppKit.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSRegularExpression+ExtAppKit.h"

@implementation NSRegularExpression (ExtAppKit)

+ (NSRegularExpression *)regularExpressionWithPattern:(NSString *)pattern {
    
    XEntry();
    
	NSError *error = nil;
	NSRegularExpression *regexpr = [NSRegularExpression regularExpressionWithPattern:pattern
																			 options:NSRegularExpressionCaseInsensitive
																			   error:&error];
	
	XLogErrorObject(error);
	
	XRet(regexpr);
}

@end
