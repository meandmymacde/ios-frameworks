//
//  NSObject+ExtensionProperties.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 20.01.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ExtensionProperties)

- (void)setValue:(id)value forPropertyKey:(NSString *)propertyKey;
- (id)valueForPropertyKey:(NSString *)propertyKey;

@end
