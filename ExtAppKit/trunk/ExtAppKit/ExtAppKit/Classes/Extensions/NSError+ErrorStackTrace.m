//
//  NSError+ErrorStackTrace.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.03.13.
//  Copyright (c) 2013 Javier Soto
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//  IN THE SOFTWARE.
//

#import <objc/runtime.h>
#import "NSError+ErrorStackTrace.h"

@implementation NSError (ErrorStackTrace)

@dynamic stackTrace;

- (void)setStackTrace:(NSString *)js_stackTrace
{
    objc_setAssociatedObject(self, @"stackTrace", js_stackTrace, OBJC_ASSOCIATION_COPY);
}

- (NSString *)stackTrace
{
    return objc_getAssociatedObject(self, @"stackTrace");
}

#pragma mark - Swizzled Method

- (id)init_jsswizzledInitWithDomain:(NSString *)domain code:(NSInteger)code userInfo:(NSDictionary *)dict
{
    // Call original implementation
    if ((self = [self init_jsswizzledInitWithDomain:domain code:code userInfo:dict]))
    {
        const NSUInteger linesToRemoveInStackTrace = 1; // This init method
        
        NSArray *stacktrace = [NSThread callStackSymbols];
        stacktrace = [stacktrace subarrayWithRange:NSMakeRange(linesToRemoveInStackTrace, stacktrace.count - linesToRemoveInStackTrace)];
        
        self.stackTrace = [stacktrace description];
    }
    
    return self;
}

@end


static void __attribute__((constructor)) JSErrorStackTraceInstall()
{
    Class NSErrorClass = [NSError class];
    
    Method originalInitMethod = class_getInstanceMethod(NSErrorClass, @selector(initWithDomain:code:userInfo:));
    Method swizzledInitMethod = class_getInstanceMethod(NSErrorClass, @selector(init_jsswizzledInitWithDomain:code:userInfo:));
    
    method_exchangeImplementations(originalInitMethod, swizzledInitMethod);
}
