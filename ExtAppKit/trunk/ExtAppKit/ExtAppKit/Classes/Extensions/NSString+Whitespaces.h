//
//  NSString+Whitespaces.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Whitespaces)

- (NSInteger)trimmedLength;

@end
