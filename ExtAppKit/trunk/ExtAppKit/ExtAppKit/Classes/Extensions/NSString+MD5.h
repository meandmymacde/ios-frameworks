//
//  NSString+MD5.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 31.05.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
 @class NSString (MD5)
 @abstract This category extends NSString with a method for generating a 
    MD5 hash for the string.
 */
@interface NSString (MD5)

/*!
 This method computes the MD5 hash value for the given string.
 @result The MD5 hash value as a string.
 */
- (NSString *)MD5;

@end
