//
//  NSObject+ExtensionProperties.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 20.01.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSObject+ExtensionProperties.h"

@implementation NSObject (ExtensionProperties)

- (void)setValue:(id)value forPropertyKey:(NSString *)propertyKey
{
    objc_setAssociatedObject(self, (__bridge const void *)(propertyKey), value, OBJC_ASSOCIATION_RETAIN);
}

- (id)valueForPropertyKey:(NSString *)propertyKey
{
    id value = nil;
    
    value = objc_getAssociatedObject(self, (__bridge const void *)(propertyKey));
    
    return value;
}

@end
