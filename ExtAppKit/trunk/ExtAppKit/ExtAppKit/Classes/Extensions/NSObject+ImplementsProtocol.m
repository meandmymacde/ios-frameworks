//
//  NSObject+ImplementsProtocol.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 20.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSObject+ImplementsProtocol.h"

@implementation NSObject (ImplementsProtocol)

- (BOOL)implementsProtocol:(Protocol *)aProtocol
{
    XEntry();
    
    BOOL result = YES;
    unsigned int count = 0;
    struct objc_method_description *methodList = nil;
    
    methodList = protocol_copyMethodDescriptionList(aProtocol, YES, YES, &count);
    
    if (count > 0 && methodList != NULL) {
        
        for (unsigned int i = 0; i < count; i++) {
            
            if (![self respondsToSelector:methodList[i].name]) {
                
                result = NO;
                break;
            }
        }
    }
    
    if (result) {
        
        Class myClass = [self class];
        objc_property_t *propList;
        const char *aName = NULL;
        
        count = 0;
        propList = protocol_copyPropertyList(aProtocol, &count);
        
        if (count > 0 && propList != NULL) {

            for (unsigned int i = 0; i < count; i++) {

                aName = property_getName(propList[i]);
                
                if (class_getProperty(myClass, aName) == NULL) {
                    
                    result = NO;
                    break;
                }
            }
        }
    }
    
    XRet(result);
}

@end
