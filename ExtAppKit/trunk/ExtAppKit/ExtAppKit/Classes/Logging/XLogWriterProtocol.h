//
//  XLogWriterProtocol.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol XLogWriterProtocol <NSObject>

- (void)writeLogMessage:(NSString *)logMessage;

@end
