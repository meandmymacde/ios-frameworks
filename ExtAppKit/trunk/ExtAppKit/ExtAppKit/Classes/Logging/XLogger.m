//
//  XLogger.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XLogLevel.h"
#import "XLogger.h"
#import "XLogFormatterProtocol.h"
#import "XLogWriterProtocol.h"


@implementation XLogger
{
    NSOperationQueue *loggerQueue;
}

#pragma mark - Formatter and Writer delegates

@synthesize formatterDelegate;
@synthesize writerDelegate;
@synthesize logLevel;


#pragma mark --
#pragma mark Singleton Implementation

static XLogger *_sharedInstance = nil;

+ (XLogger *)logger 
{
    @synchronized(self) {
        
        if (_sharedInstance == nil) {
            
            _sharedInstance = [[XLogger alloc] init];
        }
    }
    
    return _sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone 
{
    @synchronized(self) {
        
        if (_sharedInstance == nil) {
            
            _sharedInstance = [super allocWithZone:zone];
            
            return _sharedInstance;  // assignment and return on first allocation
        }
    }
    
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone 
{
    return self;
}


#pragma mark - Initialization

- (id)init 
{    
    if ((self = [super init]) != nil) {
        
        loggerQueue = [[NSOperationQueue alloc] init];
        loggerQueue.maxConcurrentOperationCount = 1;
        loggerQueue.suspended = NO;
        
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyyMMdd-HHmmssSSS"];
                         
        self.formatterDelegate = self;
        self.writerDelegate = self;
        self.logLevel = Info;
    }
    
    return self;
}


#pragma mark - Log methods

- (void)logLevel:(XLogLevel)level timestamp:(NSDate *)ts area:(NSString *)area format:(NSString *)format arguments:(va_list)argList 
{
    if (level >= self.logLevel) {
        
        if ([self.formatterDelegate respondsToSelector:@selector(formatLogLevel:timestamp:area:message:)]) {

            if ([self.writerDelegate respondsToSelector:@selector(writeLogMessage:)]) {
                
                NSString *message = [[NSString alloc] initWithFormat:format 
                                                            arguments:argList];
                NSString *logMessage = [self.formatterDelegate formatLogLevel:level
                                                                    timestamp:ts
                                                                         area:area
                                                                      message:message];
                
                [loggerQueue addOperationWithBlock:^(void) {
                    [self.writerDelegate writeLogMessage:logMessage];
                }];
            }
        }
    }
}

- (void)logLevel:(XLogLevel)level area:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:level timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logFatalForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Fatal timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logErrorForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Error timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logWarnForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Warn timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logInfoForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Info timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logDebugForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Debug timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}

- (void)logTraceForArea:(NSString *)area messageFormat:(NSString *)format, ...
{
    va_list argList;
    va_start(argList, format);
    
    [self logLevel:Trace timestamp:[NSDate date] area:area format:format arguments:argList];
    
    va_end(argList);
}


#pragma mark - XLogFormatterProtocol Implementation

- (NSString *)stringForLogLevel:(XLogLevel)level
{
    NSString *str = nil;
    
    switch (level) {
            
        case Trace:
            str = @"Trace";
            break;
            
        case Debug:
            str = @"Debug";
            break;
            
        case Info:
            str = @"Info";
            break;
            
        case Warn:
            str = @"Warn";
            break;
            
        case Error:
            str = @"Error";
            break;
            
        case Fatal:
            str = @"Fatal";
            break;
        
        default:
            str = @"Unknown";
            break;
    }
    
    return str;
}

- (NSString*)formatLogLevel:(XLogLevel)level 
                  timestamp:(NSDate *)ts 
                       area:(NSString *)area 
                    message:(NSString *)message 
{
    NSString *str = nil;
    NSString *levelStr = [self stringForLogLevel:level];
    NSString *tsStr = [dateFormatter stringFromDate:ts];
    
    str = [NSString stringWithFormat:@"[%@|%@|%@|%@]", tsStr, levelStr, area, message];
    
    return str;
}


#pragma mark - XLogWriterProtocol Implementation

- (void)writeLogMessage:(NSString *)logMessage 
{
    NSLog(@"%@", logMessage);
}

@end
