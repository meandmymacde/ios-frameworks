//
//  XLogFormatterProtocol.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XLogEntry;


@protocol XLogFormatterProtocol <NSObject>

- (NSString*)formatLogLevel:(XLogLevel)level 
                  timestamp:(NSDate *)ts 
                       area:(NSString *)area 
                    message:(NSString *)message;

@end
