//
//  XLogLevel.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 24.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#ifndef ExtAppKit_XLogLevel_h
#define ExtAppKit_XLogLevel_h

typedef enum 
{
    Trace   = 1,
    Debug   = 2,
    Info    = 4,
    Warn    = 8,
    Error   = 16,
    Fatal   = 32,
    None    = 0xffff
} XLogLevel;

#endif
