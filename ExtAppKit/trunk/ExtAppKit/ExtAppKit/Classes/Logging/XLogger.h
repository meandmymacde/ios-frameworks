//
//  XLogger.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 23.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XLogFormatterProtocol;
@protocol XLogWriterProtocol;


// Define some convenience macros
#define XLog(logLevel, area, ...) {[[XLogger logger] logLevel:logLevel area:area messageFormat:__VA_ARGS__];}
#define XLogFatal(area, ...) {[[XLogger logger] logFatalForArea:area messageFormat:__VA_ARGS__];}
#define XLogError(area, ...) {[[XLogger logger] logErrorForArea:area messageFormat:__VA_ARGS__];}
#define XLogWarn(area, ...) {[[XLogger logger] logWarnForArea:area messageFormat:__VA_ARGS__];} 
#define XLogInfo(area, ...) {[[XLogger logger] logInfoForArea:area messageFormat:__VA_ARGS__];}
#define XLogDebug(area, ...) {[[XLogger logger] logDebugForArea:area messageFormat:__VA_ARGS__];}
#define XLogTrace(area, ...) {[[XLogger logger] logTraceForArea:area messageFormat:__VA_ARGS__];}

#define XLogErrorObject(error) {if (error != nil) {XLogError([error domain], @"Error Object = %@", error);}}

#define XEntry()    {XLogTrace(@"MethodTrace", @"Method Entry <%@:%@:%d>", NSStringFromClass([self class]), NSStringFromSelector(_cmd), __LINE__);}
#define XRet(val)   {XLogTrace(@"MethodTrace", @"Method Return Value <%@:%@:%d>", NSStringFromClass([self class]), NSStringFromSelector(_cmd), __LINE__); return val;}
#define XLeave()    {XLogTrace(@"MethodTrace", @"Method Leave <%@:%@:%d>", NSStringFromClass([self class]), NSStringFromSelector(_cmd), __LINE__);}


/*!
 @class XLogger
 @abstract This class povides an easy to use logging mechanism.
 @discuss You can change the format of the log entries by setting your own
          formaterDelegate. Additionally you can route the log entries to another
          file by setting your own writerDelegate.
 */
@interface XLogger : NSObject<XLogFormatterProtocol, XLogWriterProtocol>
{
    NSDateFormatter *dateFormatter;
}

#pragma mark - Properties

/*!
 @var formatterDelegate
 @abstract Delegate for formating log messages.
 */
@property (nonatomic, unsafe_unretained) id<XLogFormatterProtocol> formatterDelegate;

/*!
 @var writerDelegate
 @abstract Delegate for writing formatted log messages.
 */
@property (nonatomic, unsafe_unretained) id<XLogWriterProtocol> writerDelegate;

/*!
 @var logLevel
 @abstract The current log level. Default: Info.
 @discuss All messages with a log level >= this log level are written
 */
@property (nonatomic, assign) XLogLevel logLevel;

#pragma mark - Initialization

+ (XLogger *)logger;

#pragma mark - Log methods

- (void)logLevel:(XLogLevel)level area:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logFatalForArea:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logErrorForArea:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logWarnForArea:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logInfoForArea:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logDebugForArea:(NSString *)area messageFormat:(NSString *)format, ...;
- (void)logTraceForArea:(NSString *)area messageFormat:(NSString *)format, ...;

@end
