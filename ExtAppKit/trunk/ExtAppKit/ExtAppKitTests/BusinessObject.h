//
//  BusinessObject.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 19.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "BusinesObjectProtocol.h"

@interface BusinessObject : XGenericBusinessObject<BusinesObjectProtocol>

@end
