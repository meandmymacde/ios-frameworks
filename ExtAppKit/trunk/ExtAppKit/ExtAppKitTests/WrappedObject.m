//
//  WrappedObject.m
//  ExtAppKit
//
//  Created by Thomas Bonk on 19.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "WrappedObject.h"

@implementation WrappedObject

@synthesize value;
@synthesize valueUpper;
- (NSString *)valueUpper
{
    return [self.value uppercaseString];
}

- (void)sayHi 
{
    NSLog(@"Hi! Value is %@ (%@)", self.value, self.valueUpper);
}

- (void)sayHiTo:(NSString *)name
{
    NSLog(@"Hi %@! Value is %@ (%@)", name, self.value, self.valueUpper);
}

@end
