//
//  WrappedObject.h
//  ExtAppKit
//
//  Created by Thomas Bonk on 19.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WrappedObject : NSObject

@property (nonatomic, retain) NSString *value;
@property (nonatomic, readonly) NSString *valueUpper;

- (void)sayHi;
- (void)sayHiTo:(NSString *)name;

@end
