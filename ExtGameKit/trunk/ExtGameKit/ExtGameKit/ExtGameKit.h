//
//  ExtGameKit.h
//  ExtGameKit
//
//  Created by Bonk, Thomas on 13.06.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ExtGameKit/XGameBoardProtocol.h>
#import <ExtGameKit/XMiniMax.h>
