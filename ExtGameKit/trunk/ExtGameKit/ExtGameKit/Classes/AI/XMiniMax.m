//
//  XMiniMax.m
//  ExtGameKit
//
//  Created by Bonk, Thomas on 13.06.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <ExtAppKit/ExtAppKit.h>
#import "XMiniMax.h"

@implementation XMiniMax

+(NSInteger)minimaxWithState:(id<XGameBoardProtocol>)board
                   forPlayer:(XGameBoardPlayer)player
                 searchDepth:(int)depth
{
    XEntry();
    
    if (depth <= 0 || [board isEndOfGame]) {
        
        NSInteger score = [board evaluateBoardForPlayer:player];
        
        return score;
    }
    
    NSInteger score = NSIntegerMin; // value for loss
    NSArray *moves = [board calculateMovesForPlayer:player];
    
    for (id<XGameBoardMoveProtocol> m in moves) {
        
        id<XGameBoardProtocol> nextBoard = [board boardForMove:m];
        NSInteger sc = -[self minimaxWithState:nextBoard
                                     forPlayer:(nextBoard.maximumPlayers - player)
                                   searchDepth:(depth - 1)];
        
        if (sc > score) {
            
            score = sc;
        }
    }
    
    XRet(score);
}

+(id<XGameBoardMoveProtocol>)nextMoveForState:(id<XGameBoardProtocol>)board
                                       player:(XGameBoardPlayer)player
                                  searchDepth:(int)depth
{
    XEntry();
    
    if (depth <= 0 || [board isEndOfGame]) {
        
        return nil;
    }
    
    NSInteger score = NSIntegerMin; // value for loss
    NSArray *moves = [board calculateMovesForPlayer:player];
    id<XGameBoardMoveProtocol> calculatedMove = nil;
    
    for (id<XGameBoardMoveProtocol> m in moves) {
        
        id<XGameBoardProtocol> nextBoard = [board boardForMove:m];
        NSInteger sc = -[self minimaxWithState:nextBoard
                                     forPlayer:(nextBoard.maximumPlayers - player)
                                   searchDepth:(depth - 1)];
        
        if (sc > score) {
            
            score = sc;
            calculatedMove = m;
            calculatedMove.score = score;
        }
        else if(calculatedMove == nil) {
            
            calculatedMove = m;
            calculatedMove.score = sc;
        }
    }
    
    XRet(calculatedMove);
}

@end
