//
//  XMiniMax.h
//  ExtGameKit
//
//  Created by Bonk, Thomas on 13.06.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XMiniMax : NSObject

+(id<XGameBoardMoveProtocol>)nextMoveForState:(id<XGameBoardProtocol>)board
                                       player:(XGameBoardPlayer)player
                                  searchDepth:(int)depth;

@end
