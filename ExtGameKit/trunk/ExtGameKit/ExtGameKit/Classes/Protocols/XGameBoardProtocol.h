//
//  XGameBoardProtocol.h
//  ExtGameKit
//
//  Created by Bonk, Thomas on 13.06.13.
//  Copyright (c) 2013 Thomas Bonk Softwareentwicklung / meandmymac.de. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Typedefs

typedef unsigned char XGameBoardPlayer;


#pragma mark - Protocols

@protocol XGameBoardMoveProtocol <NSObject>

#pragma mark - Properties

@property (nonatomic, assign) XGameBoardPlayer player;
@property (nonatomic, assign) NSInteger score;

@end


@protocol XGameBoardProtocol <NSObject>

#pragma mark - Properties

@property (nonatomic, assign) XGameBoardPlayer maximumPlayers;


#pragma mark - Board related methods

- (BOOL)isEndOfGame;
- (NSInteger)evaluateBoardForPlayer:(XGameBoardPlayer)player;
- (NSArray *)calculateMovesForPlayer:(XGameBoardPlayer)player;
- (id<XGameBoardProtocol>)boardForMove:(id<XGameBoardMoveProtocol>)move;

@end
