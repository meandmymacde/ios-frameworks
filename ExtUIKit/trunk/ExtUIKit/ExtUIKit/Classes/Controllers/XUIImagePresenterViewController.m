//
//  XUIImagePresenterViewController.m
//  ExtUIKit
//
//  Created by Bonk, Thomas on 09.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XUIImagePresenterViewController.h"
#import "NSBundle+ExtUIRessources.h"


@implementation XUIImagePresenterViewController
{
    NSUInteger __currentIndex;
    NSInteger __toolbarVisibility;
}


#pragma mark - Class Variables

static NSInteger const kToolbarVisible = 1;
static NSInteger const kToolbarHidden = -1;


#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    XEntry();

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self != nil) {

        self->__currentIndex = 0;
        self->__toolbarVisibility = kToolbarHidden;
    }

    XRet(self);
}

+ (XUIImagePresenterViewController *)imagePresenterViewController
{
    XEntry();
    
    XUIImagePresenterViewController *controller = nil;

    controller = [[XUIImagePresenterViewController alloc] initWithNibName:@"XUIImagePresenterView"
                                                                   bundle:[NSBundle ExtUIKitBundle]];

    XRet(controller);
}


#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    XEntry();

    self.scrollView.minimumZoomScale = 0.1;
    self.scrollView.maximumZoomScale = 10;
    self.scrollView.zoomScale = 5.0;

    XLeave();
}

- (void)viewWillAppear:(BOOL)animated
{
    XEntry();

    [self showImageAtIndex:self->__currentIndex];

    XLeave();
}


#pragma mark - Image Display

- (void)showImageAtIndex:(NSInteger)index
{
    XEntry();

    if (index >= 0 && index < [self.dataSource numberOfImagesInImagePresenterController:self]) {

        UIImage *image = [self.dataSource imagePresenterController:self
                                                      imageAtIndex:index];

        self.imageView.image = image;
        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y,
                                          image.size.width, image.size.height);
        self->__currentIndex = index;

    }

    XLeave();
}


#pragma mark - Toolbar handling

- (void)toggleToolbar:(NSInteger)action
{
    XEntry();

/*    NSTimeInterval animationDuration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = 0.0;
    CGRect viewFrame = self.view.frame;
    CGFloat *viewYPos = nil;
    CGFloat *viewWidth = nil;
    CGFloat positionFactor = 0.0;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

    if (UIDeviceOrientationIsPortrait(orientation)) {

        keyboardHeight = keyboardFrame.size.height;
        viewYPos = &viewFrame.origin.y;
        viewWidth = &viewFrame.size.height;

        if (orientation == UIInterfaceOrientationPortraitUpsideDown) {

            positionFactor = 1.0;
        }
    }
    else {

        keyboardHeight = keyboardFrame.size.width;
        viewYPos = &viewFrame.origin.x;
        viewWidth = &viewFrame.size.width;

        if (orientation == UIInterfaceOrientationLandscapeRight) {

            positionFactor = 1.0;
        }
    }

    *viewWidth = *viewWidth - action * keyboardHeight;
    *viewYPos = *viewYPos + action * keyboardHeight * positionFactor;

    [UIView animateWithDuration:animationDuration animations:^{

        self.view.frame = viewFrame;
    }];*/

    XLeave();
}

- (void)toggleToolbar
{
    XEntry();

    switch (self->__toolbarVisibility) {

        case kToolbarHidden:
            [self toggleToolbar:kToolbarVisible];
            break;

        case kToolbarVisible:
            [self toggleToolbar:kToolbarHidden];
            break;

        default:
            // do nothing
            break;
    }

    XLeave();
}

- (IBAction)handleDone:(id)sender
{
    XEntry();

    [self dismissImagePresenterAnimated:YES];

    XLeave();
}

- (IBAction)handlePrevious:(id)sender
{
    XEntry();

    XLeave();
}


- (IBAction)handleNext:(id)sender
{
    XEntry();

    XLeave();
}


#pragma mark - Gesture Recognizer Handler

- (IBAction)handleGesture:(UIGestureRecognizer *)gestureRecognizer
{
    XEntry();

    [self toggleToolbar];

    XLeave();
}


#pragma mark - Show/Dismiss Image Presenter

- (UIWindow *)keyWindow
{
    XEntry();

    UIApplication *app = [UIApplication sharedApplication];
    UIWindow *window = app.keyWindow;

    XRet(window);
}

- (void)showImagePresenterForIndex:(NSUInteger)index animated:(BOOL)animated
{
    XEntry();

    UIViewController *rootViewController = [self keyWindow].rootViewController;

    XLogDebug(NSStringFromClass([self class]), @"keyWindow = %@", [self keyWindow]);
    XLogDebug(NSStringFromClass([self class]), @"rootViewController = %@", rootViewController);

    self.modalPresentationStyle = UIModalPresentationFullScreen;
    self->__currentIndex = index;
    [rootViewController presentViewController:self
                                     animated:animated
                                   completion:^{}];

    XLeave();
}

- (void)showImagePresenterAnimated:(BOOL)animated
{
    XEntry();

    [self showImagePresenterForIndex:0 animated:animated];

    XLeave();
}

- (void)dismissImagePresenterAnimated:(BOOL)animated
{
    XEntry();

    [self dismissViewControllerAnimated:animated completion:^{}];

    // TODO: Call Delegate

    XLeave();
}

@end
