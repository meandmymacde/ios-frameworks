//
//  XUITabBarController.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XUITabBarController : UITabBarController<UIActionSheetDelegate>

#pragma mark - Properties

/*!
 @var tabBarVisible
 @abstract Sets a flag which makes the tab bar visible or hidden.
 */
@property (nonatomic, assign) bool tabBarVisible;


#pragma mark - Initialization

/*!
 Returns a newly initialized view controller with the nib file in the specified bundle.
 
 @param nibName
    The name of the nib file to associate with the view controller. The nib file
    name should not contain any leading path information. If you specify nil, 
    the nibName property is set to nil.
 @param nibBundle
    The bundle in which to search for the nib file. This method looks for the 
    nib file in the bundle's language-specific project directories first, 
    followed by the Resources directory. If nil, this method looks for the nib 
    file in the main bundle.
 
 @result A newly initialized XTabBarController object.
 
 @discussion This is the designated initializer for this class.
 
 The nib file you specify is not loaded right away. It is loaded the first time 
 the view controller’s view is accessed. If you want to perform additional 
 initialization after the nib file is loaded, override the viewDidLoad method 
 and perform your tasks there.
 
 If you specify nil for the nibName parameter and you do not override the 
 loadView method, the view controller searches for a nib file using other means. 
 See nibName.
 
 If your app uses a storyboard to define a view controller and its associated 
 views, your app never initializes objects of that class directly. Instead, view
 controllers are either instantiated by the storyboard—either automatically by 
 iOS when a segue is triggered or programmatically when your app calls the 
 storyboard object’s instantiateViewControllerWithIdentifier: method. When 
 instantiating a view controller from a storyboard, iOS initializes the new view 
 controller by calling its initWithCoder: method instead. iOS automatically sets 
 the nibName property to a nib file stored inside the storyboard.
 
 For more information about how a view controller loads its view, 
 see “The View Controller Life Cycle”.
 */
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

/*!
 Returns an object initialized from data in a given unarchiver.
 
 @param decoder
    An unarchiver object.
 
 @result self, initialized using the data in decoder. 
 */
- (id)initWithCoder:(NSCoder *)decoder;

/*!
 Encodes the receiver using a given archiver.
 
 @param encoder
    An archiver object.
 */
- (void)encodeWithCoder:(NSCoder *)encoder;


#pragma mark - TabBar Manipulation

- (void)showTabBar:(bool)visible animated:(bool)animated;
- (void)showTabSelectorFromBarButtonItem:(UIBarButtonItem *)barButtonItem;

@end
