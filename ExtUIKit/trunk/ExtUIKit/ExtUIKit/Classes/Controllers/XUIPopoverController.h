//
//  XUIPopoverController.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.08.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XUIPopoverController : UIPopoverController<UIPopoverControllerDelegate>

#pragma mark - Properties

@property (nonatomic, readonly, strong) XMulticastDelegate *multicastDelegate;


#pragma mark - Initialization / Deallocation

- (id)initWithContentViewController:(UIViewController*)viewController;

@end
