//
//  XUIImagePresenterViewController.h
//  ExtUIKit
//
//  Created by Bonk, Thomas on 09.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


@class XUIImagePresenterViewController;

@protocol XUIImagePresenterViewControllerDelegate <NSObject>

@optional
- (void)imagePresenterViewControllerDidDismiss:(XUIImagePresenterViewController *)controller;

@end


@protocol XUIImagePresenterViewControllerDataSource <NSObject>

@required
- (NSUInteger)numberOfImagesInImagePresenterController:(XUIImagePresenterViewController *)controller;
- (UIImage *)imagePresenterController:(XUIImagePresenterViewController *)controller
                         imageAtIndex:(NSUInteger)index;

@end


@interface XUIImagePresenterViewController : UIViewController

#pragma mark - Properties

@property (nonatomic, assign) id<XUIImagePresenterViewControllerDelegate>delegate;
@property (nonatomic, assign) id<XUIImagePresenterViewControllerDataSource>dataSource;

//@property (nonatomic, weak) IBOutlet UIToolbar *topToolbar;
@property (nonatomic, weak) IBOutlet UIToolbar *bottomToolbar;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;


#pragma mark - Initialization

+ (XUIImagePresenterViewController *)imagePresenterViewController;


#pragma mark - Show/Dismiss Image Presenter

- (void)showImagePresenterForIndex:(NSUInteger)index animated:(BOOL)animated;
- (void)showImagePresenterAnimated:(BOOL)animated;
- (void)dismissImagePresenterAnimated:(BOOL)animated;


#pragma mark - Action Handlers

- (IBAction)handleDone:(id)sender;
- (IBAction)handlePrevious:(id)sender;
- (IBAction)handleNext:(id)sender;


#pragma mark - Gesture Recognizer Handler

- (IBAction)handleGesture:(UIGestureRecognizer *)gestureRecognizer;

@end
