//
//  XUIPopoverControllerManager.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.08.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface XUIPopoverControllerManager : NSObject {

	NSMutableArray *openedPopoverControllers;
}

#pragma mark -
#pragma mark Singleton methods

+ (XUIPopoverControllerManager*)sharedInstance;


#pragma mark -
#pragma mark Managing popover controllers

+ (void)dismissAllPopovers;
- (void)add:(id)controller;
- (void)remove:(id)controller;
- (void)dismissAll;

@end
