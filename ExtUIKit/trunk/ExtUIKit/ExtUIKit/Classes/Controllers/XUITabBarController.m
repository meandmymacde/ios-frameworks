//
//  XUITabBarController.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.06.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XUITabBarController.h"
#import "XUIActionSheet.h"


@implementation XUITabBarController

#pragma mark - Properties

@synthesize tabBarVisible = __tabBarVisible;
- (void)setTabBarVisible:(bool)val
{
    XEntry();
    
    if (val != self.tabBarVisible) {
        
        __tabBarVisible = val;
        [self showTabBar:self.tabBarVisible animated:NO];
    }
    
    XLeave();
}


#pragma mark - Initialization

- (id)init
{
    XEntry();
    
    if ((self = [super init]) != nil) {
        
        __tabBarVisible = YES;
    }
    
    XRet(self);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    XEntry();
    
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) != nil) {
        
        __tabBarVisible = YES;
    }
    
    XRet(self);
}


- (id)initWithCoder:(NSCoder *)decoder 
{
    XEntry();
    
    if ((self = [super initWithCoder:decoder]) != nil) {
        
        NSNumber *val = [decoder decodeObjectForKey:@"tabBarVisible"];
        
        if (val != nil) {
            
            self.tabBarVisible = [val boolValue];
        }
        else {
            
            __tabBarVisible = YES;
        }
    }
    
    XRet(self);
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
    XEntry();
    
    [encoder encodeObject:[NSNumber numberWithBool:self.tabBarVisible]
                                            forKey:@"tabBarVisible"];
    
    XLeave();
}


#pragma mark - TabBar Manipulation

- (UITabBar *)retrieveTabBar 
{
    XEntry();
    
    UITabBar *tabBar = nil;
    
    for (UIView *view in self.view.subviews) {
        
        if([view isKindOfClass:[UITabBar class]]) {
            
            tabBar = (UITabBar *)view;
        }
    }
    
    XRet(tabBar);
}


- (void)showTabBar:(bool)visible animated:(bool)animated
{
    XEntry();
    
    __tabBarVisible = visible;
    
    if (animated) {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
    }
    
    UITabBar *tabBar = [self retrieveTabBar];
    CGRect tabBarFrame = tabBar.frame;
    CGRect newTabBarFrame;
    CGFloat factor;
    
    if (self.tabBarVisible) {
        
        // calculate sizes/positions for visible TabBar
        newTabBarFrame = CGRectMake(tabBarFrame.origin.x, tabBarFrame.origin.y-tabBarFrame.size.height-1,
                                    tabBarFrame.size.width, tabBarFrame.size.height);
        factor = -1;
    }
    else {
        
        // calculate sizes/positions for hidden TabBar
        newTabBarFrame = CGRectMake(tabBarFrame.origin.x, tabBarFrame.origin.y+tabBarFrame.size.height+1,
                                    tabBarFrame.size.width, tabBarFrame.size.height);
        factor = 1;
    }
    
    // resize TabBar
    [tabBar setFrame:newTabBarFrame];
    
    // resize other subviews
    for (UIView *view in self.view.subviews) {
        
        if(view != tabBar) {
            
            // resize view
            CGRect viewFrame = view.frame;
            CGRect newViewFrame = CGRectMake(viewFrame.origin.x, viewFrame.origin.y,
                                             viewFrame.size.width, viewFrame.size.height+factor*tabBarFrame.size.height);
            [view setFrame:newViewFrame];
        }
    }
    
    if (animated) {
        
        [UIView commitAnimations];
    }
    
    XLeave();
}


- (void)showTabSelectorFromBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    XEntry();
    
    XUIActionSheet *actionSheet = [XUIActionSheet actionSheetWithTitle:nil
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                          buttonTitles:nil];
    
    for (UITabBarItem *item in self.tabBar.items) {
        
        [actionSheet addButtonWithTitle:item.title];
    }
    [actionSheet showFromBarButtonItem:barButtonItem animated:YES];
    
    XLeave();
}


#pragma mark - ActionHandlers

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    XEntry();
    
    if (buttonIndex >= 0 && buttonIndex < self.tabBar.items.count) {
        
        self.selectedIndex = buttonIndex;
    }
    
    XLeave();
}

@end
