//
//  XPopoverControllerManager.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.08.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import "XUIPopoverControllerManager.h"
#import "XUIPopoverController.h"


static XUIPopoverControllerManager *sharedInstance = nil;


@implementation XUIPopoverControllerManager

#pragma mark -
#pragma mark Initialization / Deallocation

- (id)init {

	if (self = [super init]) {
		
		openedPopoverControllers = [[NSMutableArray alloc] init];
	}
	
	return self;
}


#pragma mark -
#pragma mark Singleton methods

+ (XUIPopoverControllerManager *)sharedInstance {
	
    @synchronized( self ) {
		
        if (sharedInstance == nil) {
			
			sharedInstance = [[XUIPopoverControllerManager alloc] init];
		}
    }
	
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
	
    @synchronized(self) {
		
        if (sharedInstance == nil) {
			
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;  // assignment and return on first allocation
        }
    }
	
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone {
	
    return self;
}


#pragma mark -
#pragma mark Managing popover controllers

- (void)add:(id)controller {

	@synchronized(openedPopoverControllers) {
		
		[openedPopoverControllers addObject:controller];
	}
}

- (void)remove:(id)controller {
	
	@synchronized(openedPopoverControllers) {
		
		if ([openedPopoverControllers containsObject:controller]) {
		
			[openedPopoverControllers removeObject:controller];
		}
	}
}

+ (void)dismissAllPopovers
{
    XEntry();
    
    [[self sharedInstance] dismissAll];
    
    XLeave();
}

- (void)dismissAll {

	for (id popover in openedPopoverControllers) {
	
		if ([popover isKindOfClass:[XUIPopoverController class]]) {
			
			[popover dismissPopoverAnimated:YES];
		}
	}
}

@end
