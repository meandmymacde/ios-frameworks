//
//  XUIPopoverController.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 28.08.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import "XUIPopoverController.h"
#import "XUIPopoverControllerManager.h"


@implementation XUIPopoverController

#pragma mark - Properties

@synthesize multicastDelegate = _multicastDelegate;

- (XMulticastDelegate *)multicastDelegate
{
    if (_multicastDelegate == nil) {
        
        _multicastDelegate = [[XMulticastDelegate alloc] init];
        [_multicastDelegate addObserver:self
                             forKeyPath:@"delegates"
                                options:NSKeyValueObservingOptionNew
                                context:nil];
    }
    
    return _multicastDelegate;
}


#pragma mark -
#pragma mark Initialization / Deallocation

- (id)initWithContentViewController:(UIViewController*)viewController 
{
	if (self = [super initWithContentViewController:viewController]) {
		
        _multicastDelegate = nil;
        
		[[XUIPopoverControllerManager sharedInstance] add:self];
        
        [self.multicastDelegate addDelegate:self];
	}
	
	return self;
}

- (void)dealloc
{
    [self.multicastDelegate removeObserver:self forKeyPath:@"delegates"];
    [self.multicastDelegate removeDelegate:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    self.delegate = (id<UIPopoverControllerDelegate>)self.multicastDelegate;
}


#pragma mark - Override Methods

- (void)dismissPopoverAnimated:(BOOL)animated
{
    [[XUIPopoverControllerManager sharedInstance] remove:self];
    [super dismissPopoverAnimated:animated];
}


#pragma mark - UIPopoverControllerDelegate Implementation

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [[XUIPopoverControllerManager sharedInstance] remove:popoverController];
}

@end
