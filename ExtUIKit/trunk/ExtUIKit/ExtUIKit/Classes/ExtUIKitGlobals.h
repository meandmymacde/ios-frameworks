//
//  Globals.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 29.06.12.
//  Copyright (c) 2012 N/A. All rights reserved.
//

/*
 *  System Versioning Preprocessor Macros
 */
#define XUISystemVersionEqualTo(v)               ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define XUISystemVersionGreaterThan(v)           ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define XUISystemVersionGreaterThanOrEqualTo(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define XUISystemVersionLessThan(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define XUISystemVersionLessThanOrEqualTo(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/*
 * System version dependant ressource name. For iOS 7 and up, "-iOS7" will be
 * added to the filename.
 */
extern NSString *XResName(NSString *resourceName);
