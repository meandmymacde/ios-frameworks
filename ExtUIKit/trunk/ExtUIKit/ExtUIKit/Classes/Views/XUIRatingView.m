//
//  XUIRatingView.m
//  Chef A-Go Go
//
//  Created by Thomas Bonk on 14.04.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import "XUIRatingView.h"
#import "UIImage+ExtUIRessources.h"
#import "NSBundle+ExtUIRessources.h"

@interface XUIRatingView (Private)

- (void)setStarImages;

@end

@implementation XUIRatingView

#pragma mark - Properties

@synthesize notSelectedImage = _notSelectedImage;
@synthesize halfSelectedImage = _halfSelectedImage;
@synthesize fullSelectedImage = _fullSelectedImage;
@synthesize rating = _rating;
@synthesize editable = _editable;
@synthesize imageViews = _imageViews;
@synthesize maxRating = _maxRating;
@synthesize midMargin = _midMargin;
@synthesize leftMargin = _leftMargin;
@synthesize minImageSize = _minImageSize;
@synthesize delegate = _delegate;

- (void)setMaxRating:(int)maxRating {
    
    XEntry();
        
    _maxRating = maxRating;
    
    // Remove old image views
    for(int i = 0; i < self.imageViews.count; ++i) {
        
        UIImageView *imageView = (UIImageView *) [self.imageViews objectAtIndex:i];
        
        [imageView removeFromSuperview];
    }
    [self.imageViews removeAllObjects];
    
    // Add new image views
    for(int i = 0; i < maxRating; ++i) {
        
        UIImageView *imageView = [[UIImageView alloc] init];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.imageViews addObject:imageView];
        [self addSubview:imageView];
    }
    
    // Relayout and refresh
    [self setNeedsLayout];
    [self refresh];
    
    XLeave();
}

- (void)setNotSelectedImage:(UIImage *)image
{
    XEntry();
    
    _notSelectedImage = image;
    [self refresh];
    
    XLeave();
}

- (void)setHalfSelectedImage:(UIImage *)image
{
    XEntry();
    
    _halfSelectedImage = image;
    [self refresh];
    
    XLeave();
}

- (void)setFullSelectedImage:(UIImage *)image {
    
    _fullSelectedImage = image;
    [self refresh];
}

- (void)setRating:(float)rating {
    
    _rating = rating;
    [self refresh];
}


#pragma mark - Initialization

- (void)baseInit {
    
    _notSelectedImage = nil;
    _halfSelectedImage = nil;
    _fullSelectedImage = nil;
    _rating = 0;
    _editable = NO;
    _imageViews = [[NSMutableArray alloc] init];
    _maxRating = 5;
    _midMargin = 5;
    _leftMargin = 0;
    _minImageSize = CGSizeMake(5, 5);
    _delegate = nil;
}

- (id)initWithFrame:(CGRect)frame {
	
	if (self = [super initWithFrame:frame]) {
		
        [self baseInit];
		[self setStarImages];
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	
	if (self = [super initWithCoder:aDecoder]) {
		
        [self baseInit];
		[self setStarImages];
	}
	
	return self;
}

- (void)setStarImages {
	
    self.notSelectedImage = [UIImage imageNamed:XResName(@"star-nonselected.png") bundleName:kExtUIKitBundleName];
    self.halfSelectedImage = [UIImage imageNamed:XResName(@"star-halfselected.png") bundleName:kExtUIKitBundleName];
    self.fullSelectedImage = [UIImage imageNamed:XResName(@"star-selected.png") bundleName:kExtUIKitBundleName];
}

- (void)dealloc {
	
    self.notSelectedImage = nil;
    self.halfSelectedImage = nil;
    self.fullSelectedImage = nil;
    self.imageViews = nil;
}


#pragma mark - Laying out Subviews

- (void)refresh {
    
    for(int i = 0; i < self.imageViews.count; ++i) {
        
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        
        if (self.rating >= i+1) {
            
            imageView.image = self.fullSelectedImage;
        } else if (self.rating > i) {
            
            imageView.image = self.halfSelectedImage;
        } else {
            
            imageView.image = self.notSelectedImage;
        }
    }
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (self.notSelectedImage == nil) {
        
        return;
    }
    
    float desiredImageWidth = (self.frame.size.width - (self.leftMargin*2) - (self.midMargin*self.imageViews.count)) / self.imageViews.count;
    float imageWidth = MAX(self.minImageSize.width, desiredImageWidth);
    float imageHeight = MAX(self.minImageSize.height, self.frame.size.height);
    
    for (int i = 0; i < self.imageViews.count; ++i) {
        
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        CGRect imageFrame = CGRectMake(self.leftMargin + i*(self.midMargin+imageWidth), 0, imageWidth, imageHeight);
        
        imageView.frame = imageFrame;
    }    
    
}


#pragma mark - Touch Detection

- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    
    if (!self.editable) {
        
        return;
    }
    
    NSUInteger newRating = 0;
    
    for(NSUInteger i = self.imageViews.count - 1; i >= 0; i--) {
        
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        
        if (touchLocation.x > imageView.frame.origin.x) {
            
            newRating = i+1;
            break;
        }
    }
    
    self.rating = newRating;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.delegate ratingView:self ratingDidChange:self.rating];
}

@end
