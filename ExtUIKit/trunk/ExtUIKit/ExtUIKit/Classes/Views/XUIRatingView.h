//
//  XUIRatingView.h
//  Chef A-Go Go
//
//  Created by Thomas Bonk on 14.04.10.
//  Copyright 2010 Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XUIRatingView;

@protocol XUIRatingViewDelegate

- (void)ratingView:(XUIRatingView *)rateView ratingDidChange:(float)rating;

@end


@interface XUIRatingView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) IBOutlet id<XUIRatingViewDelegate> delegate;

@end
