//
//  XUIActionSheet.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 10.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XUIActionSheet.h"
#import "XUIPopoverControllerManager.h"

@implementation XUIActionSheet

#pragma mark - Properties

@synthesize multicastDelegate = _multicastDelegate;

- (XMulticastDelegate *)multicastDelegate
{
    XEntry();
    
    if (_multicastDelegate == nil) {
        
        _multicastDelegate = [[XMulticastDelegate alloc] init];
        [_multicastDelegate addObserver:self
                             forKeyPath:@"delegates"
                                options:NSKeyValueObservingOptionNew
                                context:nil];
    }
    
    XRet(_multicastDelegate);
}


#pragma mark - Initialization

+ (XUIActionSheet *)actionSheetWithTitle:(NSString *)title 
                                delegate:(id<UIActionSheetDelegate>)delegate 
                       cancelButtonTitle:(NSString *)cancelButtonTitle 
                  destructiveButtonTitle:(NSString *)destructiveButtonTitle 
                            buttonTitles:(NSArray *)buttonTitles 
{
    XEntry();
    
    XUIActionSheet *actionSheet = [[XUIActionSheet alloc] initWithTitle:title
                                                               delegate:delegate
                                                      cancelButtonTitle:cancelButtonTitle
                                                 destructiveButtonTitle:destructiveButtonTitle
                                                           buttonTitles:buttonTitles];
    
    XRet(actionSheet);
}

- (id)initWithTitle:(NSString *)title 
           delegate:(id<UIActionSheetDelegate>)_delegate 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
destructiveButtonTitle:(NSString *)destructiveButtonTitle 
       buttonTitles:(NSArray *)buttonTitles
{
    XEntry();
    
    self = [super initWithTitle:title
                       delegate:self
              cancelButtonTitle:cancelButtonTitle
         destructiveButtonTitle:destructiveButtonTitle
              otherButtonTitles:nil];
    if (self != nil) {
        
        _multicastDelegate = nil;
        
        [[XUIPopoverControllerManager sharedInstance] add:self];
        
        [self.multicastDelegate addDelegate:self];
        if (_delegate != nil) {

            [self.multicastDelegate addDelegate:_delegate];
        }
                
        if (buttonTitles.count > 0) {
            
            for (NSString *buttonTitle in buttonTitles) {
                
                [self addButtonWithTitle:buttonTitle];
            }
        }
    }
    
    XRet(self);
}

- (void)dealloc
{
    XEntry();
    
    [self.multicastDelegate removeObserver:self forKeyPath:@"delegates"];
    [self.multicastDelegate removeDelegate:self];
    
    XLeave();
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    XEntry();
    
    self.delegate = (id<UIActionSheetDelegate>)self.multicastDelegate;
    
    XLeave();
}


#pragma mark - UIActionSheetDelegate Implementation

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    XEntry();
    
    [[XUIPopoverControllerManager sharedInstance] remove:actionSheet];
    
    XLeave();
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    XEntry();
    
    [[XUIPopoverControllerManager sharedInstance] remove:actionSheet];
    
    XLeave();
}

@end
