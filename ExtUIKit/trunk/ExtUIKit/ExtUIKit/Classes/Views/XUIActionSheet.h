//
//  XUIActionSheet.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 10.07.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XMulticastDelegate;


@interface XUIActionSheet : UIActionSheet<UIActionSheetDelegate>

#pragma mark - Properties

@property (nonatomic, readonly, strong) XMulticastDelegate *multicastDelegate;


#pragma mark - Initialization

+ (XUIActionSheet *)actionSheetWithTitle:(NSString *)title 
                                delegate:(id<UIActionSheetDelegate>)delegate 
                       cancelButtonTitle:(NSString *)cancelButtonTitle 
                  destructiveButtonTitle:(NSString *)destructiveButtonTitle 
                            buttonTitles:(NSArray *)buttonTitles;

- (id)initWithTitle:(NSString *)title 
           delegate:(id<UIActionSheetDelegate>)delegate 
  cancelButtonTitle:(NSString *)cancelButtonTitle 
destructiveButtonTitle:(NSString *)destructiveButtonTitle 
       buttonTitles:(NSArray *)buttonTitles;

@end
