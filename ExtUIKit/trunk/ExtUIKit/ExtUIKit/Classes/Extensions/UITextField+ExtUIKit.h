//
//  UITextField+ExtUIKit.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 23.03.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (ExtUIKit)

#pragma mark - Properties

@property (nonatomic, strong) IBOutlet NSString *key;
@property (nonatomic, strong) IBOutlet UIControl *previousControl;
@property (nonatomic, strong) IBOutlet UIControl *nextControl;

@end
