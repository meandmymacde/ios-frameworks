//
//  UIView+DataBinding.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 10.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

// Inspired by http://www.davidahouse.com/blog/2013/02/10/bindings-for-ios-apps/

#import <ExtAppKit/ExtAppKit.h>
#import <objc/runtime.h>
#import "UIView+DataBinding.h"

@implementation UIView (DataBinding)


static NSString * const UndefinedObjectsDictKey = @"UndefinedObjectsDict";


#pragma mark - Private Methods

- (NSArray *)undefinedKeys
{
    XEntry();
    
    id value = nil;
    NSDictionary *undefinedDict = [self valueForPropertyKey:UndefinedObjectsDictKey];
    
    if (undefinedDict) {

        value = [undefinedDict allKeys];
    }
    
    XRet(value);
}


#pragma mark - KVO Protocol

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    XEntry();
    
    NSMutableDictionary *undefinedDict = [self valueForPropertyKey:UndefinedObjectsDictKey];
    
    if (!undefinedDict) {
        
        undefinedDict = [[NSMutableDictionary alloc] init];
        [self setValue:undefinedDict forPropertyKey:UndefinedObjectsDictKey];
    }
    
    [undefinedDict setValue:value forKey:key];
    
    XLeave();
}

- (id)valueForUndefinedKey:(NSString *)key
{
    XEntry();
    
    id value = nil;
    NSMutableDictionary *undefinedDict = [self valueForPropertyKey:UndefinedObjectsDictKey];
    
    if (undefinedDict) {
        
        value = [undefinedDict valueForKey:key];
    }
    
    XRet(value);
}


#pragma mark - Public Methods

- (void)bindToObject:(id)obj {
    
    // first check ourselves for any bindable properties. Then process our
    // children.
    NSArray *undefinedKeys = [self undefinedKeys];
    
    if (undefinedKeys) {
        
        for (NSString *key in undefinedKeys) {
            
            // only bind things that start with the lowercase bind string
            if (([key length] > 5) && [key hasPrefix:@"bind:"]) {
                
                NSString *keyToBind = [key substringFromIndex:5];
                NSString *keyValue = [self valueForKey:key];
                
                [self setValue:[obj valueForKey:keyValue] forKey:keyToBind];
                
                // TODO: Register self to changes from the model key
                // TODO: Register model to changes from self key
            }
        }
    }
    
    for (UIView *subview in [self subviews]) {
        
        [subview bindToObject:obj];
    }
}

- (void)suspendBinding
{
    XEntry();
    
    XLeave();
}

- (void)resumeBinding
{
    XEntry();
    
    XLeave();
}

@end
