//
//  UITextView+Border.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 27.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (Border)

@property (nonatomic, assign) BOOL border;

@end
