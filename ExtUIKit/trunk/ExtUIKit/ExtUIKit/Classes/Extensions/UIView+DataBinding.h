//
//  UIView+DataBinding.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 10.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DataBinding)

#pragma mark - KVO Protocol

- (void)setValue:(id)value forUndefinedKey:(NSString *)key;
- (id)valueForUndefinedKey:(NSString *)key;


#pragma mark - Data Binding

- (void)bindToObject:(id)obj;
- (void)suspendBinding;
- (void)resumeBinding;

@end
