//
//  UIView+FindFirstResponder.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 04.10.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "UIView+FindFirstResponder.h"

@implementation UIView (FindFirstResponder)

- (UIView *)findFirstResponder
{
    XEntry();
    
    if (self.isFirstResponder) {
        
        XRet(self);
    }
    
    for (UIView *subView in self.subviews) {
        
        UIView *firstResponder = [subView findFirstResponder];
        
        if (firstResponder != nil) {
            
            XRet(firstResponder);
        }
    }
    
    XRet(nil);
}

- (UIView *)findSuperviewOfKind:(Class)aClass
{
    XEntry();
    
    if ([self isKindOfClass:aClass]) {
        
        XRet(self);
    }
    else {
    
        if (self.superview != nil) {

            XRet([self.superview findSuperviewOfKind:aClass]);
        }
    }
    
    XRet(nil);
}

@end
