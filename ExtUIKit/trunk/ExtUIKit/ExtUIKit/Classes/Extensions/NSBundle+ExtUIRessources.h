//
//  NSBundle+ExtUIRessources.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.07.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kExtUIKitBundleName;

@interface NSBundle (ExtUIRessources)

+ (NSBundle *)ExtUIKitBundle;

@end
