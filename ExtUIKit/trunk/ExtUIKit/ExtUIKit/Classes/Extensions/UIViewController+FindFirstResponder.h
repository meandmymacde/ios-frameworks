//
//  UIViewController+FindFirstResponder.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 04.10.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (FindFirstResponder)

- (UIView *)findFirstResponder;

@end
