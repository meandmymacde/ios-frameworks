//
//  UIView+RoundedCorner.m
//  ExtUIKit
//
//  Created by Bonk, Thomas on 20.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIView+RoundedCorner.h"


@implementation UIView (RoundedCorner)

- (void)setRoundedCornerWithRadius:(CGFloat)radius
{
    XEntry();

    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;

    XLeave();
}

@end
