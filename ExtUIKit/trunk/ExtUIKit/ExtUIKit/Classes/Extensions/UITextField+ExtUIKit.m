//
//  UITextField+ExtUIKit.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 23.03.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <objc/runtime.h>
#import "UITextField+ExtUIKit.h"

@implementation UITextField (ExtUIKit)

@dynamic key;
@dynamic previousControl;
@dynamic nextControl;

- (void)setKey:(NSString *)value
{
    objc_setAssociatedObject(self, @"key", value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)key
{
    return objc_getAssociatedObject(self, @"key");
}

- (void)setPreviousControl:(UIControl *)control
{
    objc_setAssociatedObject(self, @"previousControl", control, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIControl *)previousControl
{
    return objc_getAssociatedObject(self, @"previousControl");
}

- (void)setNextControl:(UIControl *)control
{
    objc_setAssociatedObject(self, @"nextControl", control, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIControl *)nextControl
{
    return objc_getAssociatedObject(self, @"nextControl");
}

@end
