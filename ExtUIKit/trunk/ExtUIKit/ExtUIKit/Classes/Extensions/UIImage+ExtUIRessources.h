//
//  UIImage+ExtUIRessources.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.07.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ExtUIRessources)

+ (UIImage *)imageNamed:(NSString *)imageName bundleName:(NSString *)bundleName;

@end
