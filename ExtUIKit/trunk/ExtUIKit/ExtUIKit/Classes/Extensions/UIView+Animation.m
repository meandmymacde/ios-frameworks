//
//  UIView+Animation.m
//  ExtUIKit
//
//  Created by Bonk, Thomas on 21.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

- (void)setHidden:(BOOL)hidden withAnimation:(UIViewAnimationOptions)animationOption
{
    XEntry();

    [UIView transitionWithView:self
                      duration:0.4
                       options:animationOption
                    animations:NULL
                    completion:NULL];
    self.hidden = hidden;

    XLeave();
}

- (void)setHiddenAnimated:(BOOL)hidden
{
    XEntry();

    [self setHidden:hidden withAnimation:UIViewAnimationOptionTransitionCrossDissolve];

    XLeave();
}

- (void)setBackgroundColorAnimated:(UIColor *)backgroundColor
{
    XEntry();

    [UIView animateWithDuration:0.4 animations:^{
        
        self.backgroundColor = backgroundColor;
    }];

    XLeave();
}

@end
