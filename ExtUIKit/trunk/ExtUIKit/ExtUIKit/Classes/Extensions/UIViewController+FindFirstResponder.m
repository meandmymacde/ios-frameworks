//
//  UIViewController+FindFirstResponder.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 04.10.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "UIViewController+FindFirstResponder.h"
#import "UIView+FindFirstResponder.h"

@implementation UIViewController (FindFirstResponder)

- (UIView *)findFirstResponder
{
    XEntry();
    
    UIView *view = [self.view findFirstResponder];
    
    XRet(view);
}

@end
