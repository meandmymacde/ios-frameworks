//
//  UIView+Animation.h
//  ExtUIKit
//
//  Created by Bonk, Thomas on 21.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animation)

- (void)setHidden:(BOOL)hidden withAnimation:(UIViewAnimationOptions)animationOption;
- (void)setHiddenAnimated:(BOOL)hidden;

- (void)setBackgroundColorAnimated:(UIColor *)backgroundColor;

@end
