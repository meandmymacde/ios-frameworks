//
//  UITextView+Border.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 27.09.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UITextView+Border.h"

@implementation UITextView (Border)

- (void)setBorder:(BOOL)border
{
    XEntry();
    
    [self setValue:@(border) forPropertyKey:@"border"];
    
    if (border) {
        
        //To make the border look very close to a UITextField
        //[self.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
        [self.layer setBorderColor:[[UIColor blackColor] CGColor]];
        [self.layer setBorderWidth:1.0];
        
        self.clipsToBounds = YES;
    }
    else {
        
        [self.layer setBorderColor:[[UIColor clearColor] CGColor]];
        [self.layer setBorderWidth:0.0];
        
        self.clipsToBounds = NO;
    }
    
    XLeave();
}

- (BOOL)border
{
    XEntry();
    
    BOOL border = [[self valueForPropertyKey:@"border"] boolValue];
    
    XRet(border);
}

@end
