//
//  UIImage+ExtUIRessources.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.07.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <ExtAppKit/ExtAppKit.h>
#import "UIImage+ExtUIRessources.h"

@implementation UIImage (ExtUIRessources)

+ (UIImage *)imageNamed:(NSString *)imageName bundleName:(NSString *)bundleName
{
	if (!bundleName) {
        
		return [UIImage imageNamed:imageName];
	}
	
	NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
	NSString *bundlePath = [resourcePath stringByAppendingPathComponent:bundleName];
	NSString *imagePath = [bundlePath stringByAppendingPathComponent:imageName];
    
	return [UIImage imageWithContentsOfFile:imagePath];
}

@end
