//
//  NSBundle+ExtUIRessources.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.07.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "NSBundle+ExtUIRessources.h"

NSString *const kExtUIKitBundleName = @"ExtUIKitRessources.bundle";

@implementation NSBundle (ExtUIRessources)

+ (NSBundle *)ExtUIKitBundle
{
	static NSBundle *extUIKitBundle = nil;
	static dispatch_once_t onceToken;
    
	dispatch_once(&onceToken, ^{
        
		NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kExtUIKitBundleName];
		extUIKitBundle = [[NSBundle alloc] initWithPath:bundlePath];
	});
    
	return extUIKitBundle;
}

@end
