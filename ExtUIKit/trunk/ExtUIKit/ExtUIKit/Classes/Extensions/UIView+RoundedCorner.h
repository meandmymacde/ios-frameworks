//
//  UIView+RoundedCorner.h
//  ExtUIKit
//
//  Created by Bonk, Thomas on 20.02.14.
//  Copyright (c) 2014 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundedCorner)

- (void)setRoundedCornerWithRadius:(CGFloat)radius;

@end
