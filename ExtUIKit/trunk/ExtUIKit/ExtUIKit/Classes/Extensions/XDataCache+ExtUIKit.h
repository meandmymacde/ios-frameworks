//
//  XDataCache+ExtUIKit.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface XDataCache (ExtUIKit)

#pragma mark - Load Global Cached Data

+ (UIImage *)imageNamed:(NSString *)imageName;

@end
