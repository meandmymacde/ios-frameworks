//
//  XDataCache+ExtUIKit.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 24.11.12.
//  Copyright (c) 2012 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XDataCache+ExtUIKit.h"

@implementation XDataCache (ExtUIKit)

#pragma mark -- Initialization

+ (void)initialize
{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:[XDataCache sharedCache]
           selector:@selector(didReceiveMemoryWarning:)
               name:UIApplicationDidReceiveMemoryWarningNotification
             object:nil];
}

- (void)didReceiveMemoryWarning:(NSNotification *)notification
{
    [[XDataCache sharedCache] removeAllObjects];
}


#pragma mark - Load Global Cached Data

+ (UIImage *)imageNamed:(NSString *)imageName
{
    XEntry();
    
    XDataCache *cache = [XDataCache sharedCache];
    UIImage *image = [cache cachedDataForCategory:@"XDataCache_GlobalImageCache"
                                              key:imageName
                                         callback:(id<XDataCacheCallbackProtocol>)cache];
    
    XRet(image);
}


#pragma mark - XDataCacheCallbackProtocol implementation

- (id)dataCache:(XDataCache *)dataCache
dataForCategory:(NSString *)category
            key:(id<NSObject>)key
       dataSize:(NSUInteger *)dataSize
{
    XEntry();
    
    id data = nil;
    
    if ([category isEqualToString:@"XDataCache_GlobalImageCache"]) {
        
        UIImage *image = [UIImage imageNamed:(NSString *)key];
        
        data = image;
        *dataSize = image.size.width * image.size.height * 3;
    }
    
    XRet(data);
}

@end
