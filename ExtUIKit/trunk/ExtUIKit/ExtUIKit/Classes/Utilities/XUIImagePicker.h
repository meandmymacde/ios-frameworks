//
//  XUIImagePicker.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 05.11.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>


@class XUIImagePicker;
@protocol XUIImagePickerDelegate <NSObject>

- (void)imagePicker:(XUIImagePicker *)imagePicker didFinishWithImage:(UIImage *)image;
- (void)imagePickerDidCancel:(XUIImagePicker *)imagePicker;

@end


@interface XUIImagePicker : NSObject<UIImagePickerControllerDelegate,
                                     UINavigationControllerDelegate>

#pragma mark - Static Utility methods

+ (BOOL)isCameraAvailable;
+ (BOOL)isPhotoLibraryAvailable;
+ (BOOL)isSavedPhotoAlbumAvailable;


#pragma mark - Properties

@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, assign) id<XUIImagePickerDelegate> delegate;


#pragma mark - Initialization

- (id)initWithDelegate:(id<XUIImagePickerDelegate>)delegate;


#pragma mark - Show Image Picker

- (void)pickFromPhotoLibrary;
- (void)takePhoto;

@end
