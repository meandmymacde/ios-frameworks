//
//  XUIAppearanceLoader.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 30.06.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <ExtAppKit/ExtAppKit.h>
#import <UIKit/UIKit.h>
#import "XUIAppearanceLoader.h"

@interface XUIAppearanceLoader ()

+ (id)appearanceFromClassName:(NSString *)className;
+ (void)applyAppearanceDescription:(NSDictionary *)appearanceDescription
                      toAppearance:(id)appearance;

@end

@implementation XUIAppearanceLoader

#pragma mark Apply Appearances 

+ (void)applyAppearanceFromURL:(NSURL *)url
{
    XEntry();
    
    NSDictionary *appearanceDescriptions = [NSDictionary dictionaryWithContentsOfURL:url];
    
    for (NSString *className in appearanceDescriptions.allKeys) {
        
        id appearance = [self appearanceFromClassName:className];
        
        if (appearance != nil) {
            
            NSDictionary *appearanceDescription = appearanceDescriptions[className];
            
            [self applyAppearanceDescription:appearanceDescription
                                toAppearance:appearance];
        }
    }
    
    XLeave();
}


#pragma mark - Utility mathods

+ (id)appearanceFromClassName:(NSString *)className
{
    XEntry();
    
    id appearance = nil;
    Class aClass = NSClassFromString(className);
    
    if ([aClass respondsToSelector:@selector(appearance)]) {
        
        appearance = [aClass appearance];
    }
    
    XRet(appearance);
}


+ (SEL)setterForProperty:(NSString *)property
{
    XEntry();

    NSString *capitalizedProperty = [property stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                      withString:[[property  substringToIndex:1] capitalizedString]];
    NSString *setterName = [NSString stringWithFormat:@"set%@:", capitalizedProperty];
    SEL setterSelector = NSSelectorFromString(setterName);
    
    XRet(setterSelector);
}


+ (void)applyAppearanceDescription:(NSDictionary *)appearanceDescription
                      toAppearance:(id)appearance
{
    XEntry();
    
    for (NSString *property in appearanceDescription.allKeys) {
        
        NSString *colorName = appearanceDescription[property];
        UIColor *color = [UIColor valueForKey:colorName];
        SEL setterSelector = [self setterForProperty:property];

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [appearance performSelector:setterSelector withObject:color];
        #pragma clang diagnostic pop
    }
    
    XLeave();
}

@end
