//  XUIImagePickerFacade.m
//  ExtUIKit
//
//  Created by Thomas Bonk on 05.11.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import "XUIImagePicker.h"
#import "XUIPopoverController.h"

@implementation XUIImagePicker

#pragma mark - Static Utility methods

+ (BOOL)isCameraAvailable
{
    XEntry();
    
    BOOL result = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    XRet(result);
}

+ (BOOL)isPhotoLibraryAvailable
{
    XEntry();
    
    BOOL result = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    XRet(result);
}

+ (BOOL)isSavedPhotoAlbumAvailable
{
    XEntry();
    
    BOOL result = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    XRet(result);
}


#pragma mark - Initialization

- (id)initWithDelegate:(id<XUIImagePickerDelegate>)delegate
{
    XEntry();
    
    self = [super init];
    if (self) {
        
        self.imagePickerController = [[UIImagePickerController alloc] init];
        self.imagePickerController.delegate = self;
        self.imagePickerController.preferredContentSize = CGSizeMake(600, 600);
        self.delegate = delegate;
    }
    
    XRet(self);
}

- (void)dealloc
{
    XEntry();
    
    self.imagePickerController = nil;
    self.delegate = nil;
    
    XLeave();
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    XEntry();
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    [self.delegate imagePicker:self didFinishWithImage:image];
    [picker dismissViewControllerAnimated:YES
                               completion:^{}];
    
    XLeave();
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    XEntry();
    
    [self.delegate imagePickerDidCancel:self];
    [picker dismissViewControllerAnimated:YES
                               completion:^{}];
    
    XLeave();
}


#pragma mark - Show Image Picker

- (UIWindow *)keyWindow
{
    XEntry();
    
    UIApplication *app = [UIApplication sharedApplication];
    UIWindow *window = app.keyWindow;
    
    XRet(window);
}

- (void)pickFromPhotoLibrary
{
    XEntry();
    
    UIViewController *rootViewController = [self keyWindow].rootViewController;
    
    XLogDebug(NSStringFromClass([self class]), @"keyWindow = %@", [self keyWindow]);
    XLogDebug(NSStringFromClass([self class]), @"rootViewController = %@", rootViewController);

    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePickerController.modalPresentationStyle = UIModalPresentationFormSheet;
    [rootViewController presentViewController:self.imagePickerController
                                     animated:YES
                                   completion:^{}];
    
    XLeave();
}

- (void)takePhoto
{
    XEntry();
    
    UIViewController *rootViewController = [self keyWindow].rootViewController;
    
    XLogDebug(NSStringFromClass([self class]), @"keyWindow = %@", [self keyWindow]);
    XLogDebug(NSStringFromClass([self class]), @"rootViewController = %@", rootViewController);
    
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePickerController.modalPresentationStyle = UIModalPresentationFormSheet;
    [rootViewController presentViewController:self.imagePickerController
                                     animated:YES
                                   completion:^{}];
    
    XLeave();
}

@end
