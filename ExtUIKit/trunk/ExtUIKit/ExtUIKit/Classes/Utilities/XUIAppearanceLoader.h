//
//  XUIAppearanceLoader.h
//  ExtUIKit
//
//  Created by Thomas Bonk on 30.06.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XUIAppearanceLoader : NSObject

+ (void)applyAppearanceFromURL:(NSURL *)url;

@end
