//
//  Globals.mm
//  ExtAppKit
//
//  Created by Thomas Bonk on 11.06.13.
//  Copyright (c) 2013 meandmymac.de / Thomas Bonk. All rights reserved.
//

#include "ExtUIKitGlobals.h"

/*
 * System version dependant ressource name. For iOS 7 and up, "-iOS7" will be
 * added to the filename.
 */
NSString *XResName(NSString *resourceName)
{
    if (XUISystemVersionGreaterThanOrEqualTo(@"7.0")) {
        
        NSString *name = [resourceName stringByDeletingPathExtension];
        NSString *extension = [resourceName pathExtension];
        NSString *result = nil;
        
        if (extension.length > 0) {
        
            result = [NSString stringWithFormat:@"%@-iOS7.%@", name, extension];
        }
        else {
            
            result = [NSString stringWithFormat:@"%@-iOS7", resourceName];
        }
        
        return result;
    }
    else {
        
        return resourceName;
    }
}
